(function () {
  function $parcel$interopDefault(a) {
    return a && a.__esModule ? a.default : a;
  } // ASSET: /home/himanshu/Downloads/Javascript/complete-javascript-course/18-forkify/starter/node_modules/@parcel/runtime-js/lib/bundle-manifest.js
  var $bd72ee1865b930c1fed8ae47f35e91$var$mapping = {};
  function $bd72ee1865b930c1fed8ae47f35e91$var$register(pairs) {
    var keys = Object.keys(pairs);
    for (var i = 0; i < keys.length; i++) {
      $bd72ee1865b930c1fed8ae47f35e91$var$mapping[keys[i]] = pairs[keys[i]];
    }
  }
  function $bd72ee1865b930c1fed8ae47f35e91$var$resolve(id) {
    var resolved = $bd72ee1865b930c1fed8ae47f35e91$var$mapping[id];
    if (resolved == null) {
      throw new Error('Could not resolve bundle with id ' + id);
    }
    return resolved;
  }
  var $bd72ee1865b930c1fed8ae47f35e91$export$register = $bd72ee1865b930c1fed8ae47f35e91$var$register;
  var $bd72ee1865b930c1fed8ae47f35e91$export$resolve = $bd72ee1865b930c1fed8ae47f35e91$var$resolve;
  // ASSET: /home/himanshu/Downloads/Javascript/complete-javascript-course/18-forkify/starter/node_modules/@parcel/runtime-js/lib/JSRuntime.js
  $bd72ee1865b930c1fed8ae47f35e91$export$register(JSON.parse("{\"20a8cf28ae6a204b\":\"controller.9c9e4af0.js\",\"3eec49d8191ebba7\":\"icons.c781f215.svg\"}")); // ASSET: /home/himanshu/Downloads/Javascript/complete-javascript-course/18-forkify/starter/src/js/model.js
  // ASSET: /home/himanshu/Downloads/Javascript/complete-javascript-course/18-forkify/starter/src/js/config.js
  var $ad22e0a18c2bbded425be092f88aba2e$export$API_URL = 'https://forkify-api.herokuapp.com/api/v2/recipes/';
  var $ad22e0a18c2bbded425be092f88aba2e$export$TIMEOUT_TIME = 10;
  var $ad22e0a18c2bbded425be092f88aba2e$export$RESULT_PER_PAGE = 10;
  var $ad22e0a18c2bbded425be092f88aba2e$export$KEY = '9125ab40-f78e-4ebe-8183-131d0731992d'; // ASSET: /home/himanshu/Downloads/Javascript/complete-javascript-course/18-forkify/starter/src/js/helpers.js
  function $fe40ff4fe3c4c74bdad83178dfb5e92$var$_typeof(o) {
    "@babel/helpers - typeof";

    return $fe40ff4fe3c4c74bdad83178dfb5e92$var$_typeof = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (o) {
      return typeof o;
    } : function (o) {
      return o && "function" == typeof Symbol && o.constructor === Symbol && o !== Symbol.prototype ? "symbol" : typeof o;
    }, $fe40ff4fe3c4c74bdad83178dfb5e92$var$_typeof(o);
  }
  function $fe40ff4fe3c4c74bdad83178dfb5e92$var$_regeneratorRuntime() {
    /*! regenerator-runtime -- Copyright (c) 2014-present, Facebook, Inc. -- license (MIT): https://github.com/facebook/regenerator/blob/main/LICENSE */
    $fe40ff4fe3c4c74bdad83178dfb5e92$var$_regeneratorRuntime = function _regeneratorRuntime() {
      return e;
    };
    var t,
      e = {},
      r = Object.prototype,
      n = r.hasOwnProperty,
      o = Object.defineProperty || function (t, e, r) {
        t[e] = r.value;
      },
      i = "function" == typeof Symbol ? Symbol : {},
      a = i.iterator || "@@iterator",
      c = i.asyncIterator || "@@asyncIterator",
      u = i.toStringTag || "@@toStringTag";
    function define(t, e, r) {
      return Object.defineProperty(t, e, {
        value: r,
        enumerable: !0,
        configurable: !0,
        writable: !0
      }), t[e];
    }
    try {
      define({}, "");
    } catch (t) {
      define = function define(t, e, r) {
        return t[e] = r;
      };
    }
    function wrap(t, e, r, n) {
      var i = e && e.prototype instanceof Generator ? e : Generator,
        a = Object.create(i.prototype),
        c = new Context(n || []);
      return o(a, "_invoke", {
        value: makeInvokeMethod(t, r, c)
      }), a;
    }
    function tryCatch(t, e, r) {
      try {
        return {
          type: "normal",
          arg: t.call(e, r)
        };
      } catch (t) {
        return {
          type: "throw",
          arg: t
        };
      }
    }
    e.wrap = wrap;
    var h = "suspendedStart",
      l = "suspendedYield",
      f = "executing",
      s = "completed",
      y = {};
    function Generator() {}
    function GeneratorFunction() {}
    function GeneratorFunctionPrototype() {}
    var p = {};
    define(p, a, function () {
      return this;
    });
    var d = Object.getPrototypeOf,
      v = d && d(d(values([])));
    v && v !== r && n.call(v, a) && (p = v);
    var g = GeneratorFunctionPrototype.prototype = Generator.prototype = Object.create(p);
    function defineIteratorMethods(t) {
      ["next", "throw", "return"].forEach(function (e) {
        define(t, e, function (t) {
          return this._invoke(e, t);
        });
      });
    }
    function AsyncIterator(t, e) {
      function invoke(r, o, i, a) {
        var c = tryCatch(t[r], t, o);
        if ("throw" !== c.type) {
          var u = c.arg,
            h = u.value;
          return h && "object" == $fe40ff4fe3c4c74bdad83178dfb5e92$var$_typeof(h) && n.call(h, "__await") ? e.resolve(h.__await).then(function (t) {
            invoke("next", t, i, a);
          }, function (t) {
            invoke("throw", t, i, a);
          }) : e.resolve(h).then(function (t) {
            u.value = t, i(u);
          }, function (t) {
            return invoke("throw", t, i, a);
          });
        }
        a(c.arg);
      }
      var r;
      o(this, "_invoke", {
        value: function value(t, n) {
          function callInvokeWithMethodAndArg() {
            return new e(function (e, r) {
              invoke(t, n, e, r);
            });
          }
          return r = r ? r.then(callInvokeWithMethodAndArg, callInvokeWithMethodAndArg) : callInvokeWithMethodAndArg();
        }
      });
    }
    function makeInvokeMethod(e, r, n) {
      var o = h;
      return function (i, a) {
        if (o === f) throw new Error("Generator is already running");
        if (o === s) {
          if ("throw" === i) throw a;
          return {
            value: t,
            done: !0
          };
        }
        for (n.method = i, n.arg = a;;) {
          var c = n.delegate;
          if (c) {
            var u = maybeInvokeDelegate(c, n);
            if (u) {
              if (u === y) continue;
              return u;
            }
          }
          if ("next" === n.method) n.sent = n._sent = n.arg;else if ("throw" === n.method) {
            if (o === h) throw o = s, n.arg;
            n.dispatchException(n.arg);
          } else "return" === n.method && n.abrupt("return", n.arg);
          o = f;
          var p = tryCatch(e, r, n);
          if ("normal" === p.type) {
            if (o = n.done ? s : l, p.arg === y) continue;
            return {
              value: p.arg,
              done: n.done
            };
          }
          "throw" === p.type && (o = s, n.method = "throw", n.arg = p.arg);
        }
      };
    }
    function maybeInvokeDelegate(e, r) {
      var n = r.method,
        o = e.iterator[n];
      if (o === t) return r.delegate = null, "throw" === n && e.iterator.return && (r.method = "return", r.arg = t, maybeInvokeDelegate(e, r), "throw" === r.method) || "return" !== n && (r.method = "throw", r.arg = new TypeError("The iterator does not provide a '" + n + "' method")), y;
      var i = tryCatch(o, e.iterator, r.arg);
      if ("throw" === i.type) return r.method = "throw", r.arg = i.arg, r.delegate = null, y;
      var a = i.arg;
      return a ? a.done ? (r[e.resultName] = a.value, r.next = e.nextLoc, "return" !== r.method && (r.method = "next", r.arg = t), r.delegate = null, y) : a : (r.method = "throw", r.arg = new TypeError("iterator result is not an object"), r.delegate = null, y);
    }
    function pushTryEntry(t) {
      var e = {
        tryLoc: t[0]
      };
      1 in t && (e.catchLoc = t[1]), 2 in t && (e.finallyLoc = t[2], e.afterLoc = t[3]), this.tryEntries.push(e);
    }
    function resetTryEntry(t) {
      var e = t.completion || {};
      e.type = "normal", delete e.arg, t.completion = e;
    }
    function Context(t) {
      this.tryEntries = [{
        tryLoc: "root"
      }], t.forEach(pushTryEntry, this), this.reset(!0);
    }
    function values(e) {
      if (e || "" === e) {
        var r = e[a];
        if (r) return r.call(e);
        if ("function" == typeof e.next) return e;
        if (!isNaN(e.length)) {
          var o = -1,
            i = function next() {
              for (; ++o < e.length;) if (n.call(e, o)) return next.value = e[o], next.done = !1, next;
              return next.value = t, next.done = !0, next;
            };
          return i.next = i;
        }
      }
      throw new TypeError($fe40ff4fe3c4c74bdad83178dfb5e92$var$_typeof(e) + " is not iterable");
    }
    return GeneratorFunction.prototype = GeneratorFunctionPrototype, o(g, "constructor", {
      value: GeneratorFunctionPrototype,
      configurable: !0
    }), o(GeneratorFunctionPrototype, "constructor", {
      value: GeneratorFunction,
      configurable: !0
    }), GeneratorFunction.displayName = define(GeneratorFunctionPrototype, u, "GeneratorFunction"), e.isGeneratorFunction = function (t) {
      var e = "function" == typeof t && t.constructor;
      return !!e && (e === GeneratorFunction || "GeneratorFunction" === (e.displayName || e.name));
    }, e.mark = function (t) {
      return Object.setPrototypeOf ? Object.setPrototypeOf(t, GeneratorFunctionPrototype) : (t.__proto__ = GeneratorFunctionPrototype, define(t, u, "GeneratorFunction")), t.prototype = Object.create(g), t;
    }, e.awrap = function (t) {
      return {
        __await: t
      };
    }, defineIteratorMethods(AsyncIterator.prototype), define(AsyncIterator.prototype, c, function () {
      return this;
    }), e.AsyncIterator = AsyncIterator, e.async = function (t, r, n, o, i) {
      void 0 === i && (i = Promise);
      var a = new AsyncIterator(wrap(t, r, n, o), i);
      return e.isGeneratorFunction(r) ? a : a.next().then(function (t) {
        return t.done ? t.value : a.next();
      });
    }, defineIteratorMethods(g), define(g, u, "Generator"), define(g, a, function () {
      return this;
    }), define(g, "toString", function () {
      return "[object Generator]";
    }), e.keys = function (t) {
      var e = Object(t),
        r = [];
      for (var n in e) r.push(n);
      return r.reverse(), function next() {
        for (; r.length;) {
          var t = r.pop();
          if (t in e) return next.value = t, next.done = !1, next;
        }
        return next.done = !0, next;
      };
    }, e.values = values, Context.prototype = {
      constructor: Context,
      reset: function reset(e) {
        if (this.prev = 0, this.next = 0, this.sent = this._sent = t, this.done = !1, this.delegate = null, this.method = "next", this.arg = t, this.tryEntries.forEach(resetTryEntry), !e) for (var r in this) "t" === r.charAt(0) && n.call(this, r) && !isNaN(+r.slice(1)) && (this[r] = t);
      },
      stop: function stop() {
        this.done = !0;
        var t = this.tryEntries[0].completion;
        if ("throw" === t.type) throw t.arg;
        return this.rval;
      },
      dispatchException: function dispatchException(e) {
        if (this.done) throw e;
        var r = this;
        function handle(n, o) {
          return a.type = "throw", a.arg = e, r.next = n, o && (r.method = "next", r.arg = t), !!o;
        }
        for (var o = this.tryEntries.length - 1; o >= 0; --o) {
          var i = this.tryEntries[o],
            a = i.completion;
          if ("root" === i.tryLoc) return handle("end");
          if (i.tryLoc <= this.prev) {
            var c = n.call(i, "catchLoc"),
              u = n.call(i, "finallyLoc");
            if (c && u) {
              if (this.prev < i.catchLoc) return handle(i.catchLoc, !0);
              if (this.prev < i.finallyLoc) return handle(i.finallyLoc);
            } else if (c) {
              if (this.prev < i.catchLoc) return handle(i.catchLoc, !0);
            } else {
              if (!u) throw new Error("try statement without catch or finally");
              if (this.prev < i.finallyLoc) return handle(i.finallyLoc);
            }
          }
        }
      },
      abrupt: function abrupt(t, e) {
        for (var r = this.tryEntries.length - 1; r >= 0; --r) {
          var o = this.tryEntries[r];
          if (o.tryLoc <= this.prev && n.call(o, "finallyLoc") && this.prev < o.finallyLoc) {
            var i = o;
            break;
          }
        }
        i && ("break" === t || "continue" === t) && i.tryLoc <= e && e <= i.finallyLoc && (i = null);
        var a = i ? i.completion : {};
        return a.type = t, a.arg = e, i ? (this.method = "next", this.next = i.finallyLoc, y) : this.complete(a);
      },
      complete: function complete(t, e) {
        if ("throw" === t.type) throw t.arg;
        return "break" === t.type || "continue" === t.type ? this.next = t.arg : "return" === t.type ? (this.rval = this.arg = t.arg, this.method = "return", this.next = "end") : "normal" === t.type && e && (this.next = e), y;
      },
      finish: function finish(t) {
        for (var e = this.tryEntries.length - 1; e >= 0; --e) {
          var r = this.tryEntries[e];
          if (r.finallyLoc === t) return this.complete(r.completion, r.afterLoc), resetTryEntry(r), y;
        }
      },
      catch: function _catch(t) {
        for (var e = this.tryEntries.length - 1; e >= 0; --e) {
          var r = this.tryEntries[e];
          if (r.tryLoc === t) {
            var n = r.completion;
            if ("throw" === n.type) {
              var o = n.arg;
              resetTryEntry(r);
            }
            return o;
          }
        }
        throw new Error("illegal catch attempt");
      },
      delegateYield: function delegateYield(e, r, n) {
        return this.delegate = {
          iterator: values(e),
          resultName: r,
          nextLoc: n
        }, "next" === this.method && (this.arg = t), y;
      }
    }, e;
  }
  function $fe40ff4fe3c4c74bdad83178dfb5e92$var$asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) {
    try {
      var info = gen[key](arg);
      var value = info.value;
    } catch (error) {
      reject(error);
      return;
    }
    if (info.done) {
      resolve(value);
    } else {
      Promise.resolve(value).then(_next, _throw);
    }
  }
  function $fe40ff4fe3c4c74bdad83178dfb5e92$var$_asyncToGenerator(fn) {
    return function () {
      var self = this,
        args = arguments;
      return new Promise(function (resolve, reject) {
        var gen = fn.apply(self, args);
        function _next(value) {
          $fe40ff4fe3c4c74bdad83178dfb5e92$var$asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value);
        }
        function _throw(err) {
          $fe40ff4fe3c4c74bdad83178dfb5e92$var$asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err);
        }
        _next(undefined);
      });
    };
  }
  var $fe40ff4fe3c4c74bdad83178dfb5e92$var$timeout = function timeout(s) {
    return new Promise(function (_, reject) {
      setTimeout(function () {
        reject(new Error("Request took too long! Timeout after ".concat(s, " second")));
      }, s * 1000);
    });
  };

  // export const getJSON = async function(url){
  //     try{
  //         const fetchPromise =   await fetch(url);
  //         const res = await Promise.race([fetchPromise,timeout(TIMEOUT_TIME)]);
  //         const data= await res.json();
  //         if(!res.ok)
  //         throw new Error(`${data.error} ${res.status}` +' status code');
  //         return data;
  //     }
  //     catch(err){
  //         throw err;
  //     }
  // }

  // export const sendJSON = async function(url, uploadRecipe){
  //   try{
  //       const fetchPromise = fetch(url,{
  //         method: 'POST',
  //         headers: {
  //           'Content-Type' : 'application/json',
  //         },
  //         body: JSON.stringify(uploadRecipe),
  //       });

  //       const res = await Promise.race([fetchPromise,timeout(TIMEOUT_TIME)]);
  //       const data= await res.json();
  //       if(!res.ok)
  //       throw new Error(`${data.error} ${res.status}` +' status code');
  //       return data;
  //   }
  //   catch(err){
  //       throw err;
  //   }
  // }

  var $fe40ff4fe3c4c74bdad83178dfb5e92$export$AJAX = /*#__PURE__*/function () {
    var _ref = $fe40ff4fe3c4c74bdad83178dfb5e92$var$_asyncToGenerator( /*#__PURE__*/$fe40ff4fe3c4c74bdad83178dfb5e92$var$_regeneratorRuntime().mark(function _callee(url) {
      var uploadRecipe,
        fetchPromise,
        res,
        data,
        _args = arguments;
      return $fe40ff4fe3c4c74bdad83178dfb5e92$var$_regeneratorRuntime().wrap(function _callee$(_context) {
        while (1) switch (_context.prev = _context.next) {
          case 0:
            uploadRecipe = _args.length > 1 && _args[1] !== undefined ? _args[1] : undefined;
            _context.prev = 1;
            fetchPromise = uploadRecipe ? fetch(url, {
              method: 'POST',
              headers: {
                'Content-Type': 'application/json'
              },
              body: JSON.stringify(uploadRecipe)
            }) : fetch(url);
            _context.next = 5;
            return Promise.race([fetchPromise, $fe40ff4fe3c4c74bdad83178dfb5e92$var$timeout($ad22e0a18c2bbded425be092f88aba2e$export$TIMEOUT_TIME)]);
          case 5:
            res = _context.sent;
            _context.next = 8;
            return res.json();
          case 8:
            data = _context.sent;
            if (res.ok) {
              _context.next = 11;
              break;
            }
            throw new Error("".concat(data.error, " ").concat(res.status) + ' status code');
          case 11:
            return _context.abrupt("return", data);
          case 14:
            _context.prev = 14;
            _context.t0 = _context["catch"](1);
            throw _context.t0;
          case 17:
          case "end":
            return _context.stop();
        }
      }, _callee, null, [[1, 14]]);
    }));
    return function AJAX(_x) {
      return _ref.apply(this, arguments);
    };
  }();
  function $c3b5db471f8fb616d9495023ad42cee1$var$_typeof(o) {
    "@babel/helpers - typeof";

    return $c3b5db471f8fb616d9495023ad42cee1$var$_typeof = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (o) {
      return typeof o;
    } : function (o) {
      return o && "function" == typeof Symbol && o.constructor === Symbol && o !== Symbol.prototype ? "symbol" : typeof o;
    }, $c3b5db471f8fb616d9495023ad42cee1$var$_typeof(o);
  }
  function $c3b5db471f8fb616d9495023ad42cee1$var$_slicedToArray(arr, i) {
    return $c3b5db471f8fb616d9495023ad42cee1$var$_arrayWithHoles(arr) || $c3b5db471f8fb616d9495023ad42cee1$var$_iterableToArrayLimit(arr, i) || $c3b5db471f8fb616d9495023ad42cee1$var$_unsupportedIterableToArray(arr, i) || $c3b5db471f8fb616d9495023ad42cee1$var$_nonIterableRest();
  }
  function $c3b5db471f8fb616d9495023ad42cee1$var$_nonIterableRest() {
    throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method.");
  }
  function $c3b5db471f8fb616d9495023ad42cee1$var$_unsupportedIterableToArray(o, minLen) {
    if (!o) return;
    if (typeof o === "string") return $c3b5db471f8fb616d9495023ad42cee1$var$_arrayLikeToArray(o, minLen);
    var n = Object.prototype.toString.call(o).slice(8, -1);
    if (n === "Object" && o.constructor) n = o.constructor.name;
    if (n === "Map" || n === "Set") return Array.from(o);
    if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return $c3b5db471f8fb616d9495023ad42cee1$var$_arrayLikeToArray(o, minLen);
  }
  function $c3b5db471f8fb616d9495023ad42cee1$var$_arrayLikeToArray(arr, len) {
    if (len == null || len > arr.length) len = arr.length;
    for (var i = 0, arr2 = new Array(len); i < len; i++) arr2[i] = arr[i];
    return arr2;
  }
  function $c3b5db471f8fb616d9495023ad42cee1$var$_iterableToArrayLimit(r, l) {
    var t = null == r ? null : "undefined" != typeof Symbol && r[Symbol.iterator] || r["@@iterator"];
    if (null != t) {
      var e,
        n,
        i,
        u,
        a = [],
        f = !0,
        o = !1;
      try {
        if (i = (t = t.call(r)).next, 0 === l) {
          if (Object(t) !== t) return;
          f = !1;
        } else for (; !(f = (e = i.call(t)).done) && (a.push(e.value), a.length !== l); f = !0);
      } catch (r) {
        o = !0, n = r;
      } finally {
        try {
          if (!f && null != t.return && (u = t.return(), Object(u) !== u)) return;
        } finally {
          if (o) throw n;
        }
      }
      return a;
    }
  }
  function $c3b5db471f8fb616d9495023ad42cee1$var$_arrayWithHoles(arr) {
    if (Array.isArray(arr)) return arr;
  }
  function $c3b5db471f8fb616d9495023ad42cee1$var$_regeneratorRuntime() {
    /*! regenerator-runtime -- Copyright (c) 2014-present, Facebook, Inc. -- license (MIT): https://github.com/facebook/regenerator/blob/main/LICENSE */
    $c3b5db471f8fb616d9495023ad42cee1$var$_regeneratorRuntime = function _regeneratorRuntime() {
      return e;
    };
    var t,
      e = {},
      r = Object.prototype,
      n = r.hasOwnProperty,
      o = Object.defineProperty || function (t, e, r) {
        t[e] = r.value;
      },
      i = "function" == typeof Symbol ? Symbol : {},
      a = i.iterator || "@@iterator",
      c = i.asyncIterator || "@@asyncIterator",
      u = i.toStringTag || "@@toStringTag";
    function define(t, e, r) {
      return Object.defineProperty(t, e, {
        value: r,
        enumerable: !0,
        configurable: !0,
        writable: !0
      }), t[e];
    }
    try {
      define({}, "");
    } catch (t) {
      define = function define(t, e, r) {
        return t[e] = r;
      };
    }
    function wrap(t, e, r, n) {
      var i = e && e.prototype instanceof Generator ? e : Generator,
        a = Object.create(i.prototype),
        c = new Context(n || []);
      return o(a, "_invoke", {
        value: makeInvokeMethod(t, r, c)
      }), a;
    }
    function tryCatch(t, e, r) {
      try {
        return {
          type: "normal",
          arg: t.call(e, r)
        };
      } catch (t) {
        return {
          type: "throw",
          arg: t
        };
      }
    }
    e.wrap = wrap;
    var h = "suspendedStart",
      l = "suspendedYield",
      f = "executing",
      s = "completed",
      y = {};
    function Generator() {}
    function GeneratorFunction() {}
    function GeneratorFunctionPrototype() {}
    var p = {};
    define(p, a, function () {
      return this;
    });
    var d = Object.getPrototypeOf,
      v = d && d(d(values([])));
    v && v !== r && n.call(v, a) && (p = v);
    var g = GeneratorFunctionPrototype.prototype = Generator.prototype = Object.create(p);
    function defineIteratorMethods(t) {
      ["next", "throw", "return"].forEach(function (e) {
        define(t, e, function (t) {
          return this._invoke(e, t);
        });
      });
    }
    function AsyncIterator(t, e) {
      function invoke(r, o, i, a) {
        var c = tryCatch(t[r], t, o);
        if ("throw" !== c.type) {
          var u = c.arg,
            h = u.value;
          return h && "object" == $c3b5db471f8fb616d9495023ad42cee1$var$_typeof(h) && n.call(h, "__await") ? e.resolve(h.__await).then(function (t) {
            invoke("next", t, i, a);
          }, function (t) {
            invoke("throw", t, i, a);
          }) : e.resolve(h).then(function (t) {
            u.value = t, i(u);
          }, function (t) {
            return invoke("throw", t, i, a);
          });
        }
        a(c.arg);
      }
      var r;
      o(this, "_invoke", {
        value: function value(t, n) {
          function callInvokeWithMethodAndArg() {
            return new e(function (e, r) {
              invoke(t, n, e, r);
            });
          }
          return r = r ? r.then(callInvokeWithMethodAndArg, callInvokeWithMethodAndArg) : callInvokeWithMethodAndArg();
        }
      });
    }
    function makeInvokeMethod(e, r, n) {
      var o = h;
      return function (i, a) {
        if (o === f) throw new Error("Generator is already running");
        if (o === s) {
          if ("throw" === i) throw a;
          return {
            value: t,
            done: !0
          };
        }
        for (n.method = i, n.arg = a;;) {
          var c = n.delegate;
          if (c) {
            var u = maybeInvokeDelegate(c, n);
            if (u) {
              if (u === y) continue;
              return u;
            }
          }
          if ("next" === n.method) n.sent = n._sent = n.arg;else if ("throw" === n.method) {
            if (o === h) throw o = s, n.arg;
            n.dispatchException(n.arg);
          } else "return" === n.method && n.abrupt("return", n.arg);
          o = f;
          var p = tryCatch(e, r, n);
          if ("normal" === p.type) {
            if (o = n.done ? s : l, p.arg === y) continue;
            return {
              value: p.arg,
              done: n.done
            };
          }
          "throw" === p.type && (o = s, n.method = "throw", n.arg = p.arg);
        }
      };
    }
    function maybeInvokeDelegate(e, r) {
      var n = r.method,
        o = e.iterator[n];
      if (o === t) return r.delegate = null, "throw" === n && e.iterator.return && (r.method = "return", r.arg = t, maybeInvokeDelegate(e, r), "throw" === r.method) || "return" !== n && (r.method = "throw", r.arg = new TypeError("The iterator does not provide a '" + n + "' method")), y;
      var i = tryCatch(o, e.iterator, r.arg);
      if ("throw" === i.type) return r.method = "throw", r.arg = i.arg, r.delegate = null, y;
      var a = i.arg;
      return a ? a.done ? (r[e.resultName] = a.value, r.next = e.nextLoc, "return" !== r.method && (r.method = "next", r.arg = t), r.delegate = null, y) : a : (r.method = "throw", r.arg = new TypeError("iterator result is not an object"), r.delegate = null, y);
    }
    function pushTryEntry(t) {
      var e = {
        tryLoc: t[0]
      };
      1 in t && (e.catchLoc = t[1]), 2 in t && (e.finallyLoc = t[2], e.afterLoc = t[3]), this.tryEntries.push(e);
    }
    function resetTryEntry(t) {
      var e = t.completion || {};
      e.type = "normal", delete e.arg, t.completion = e;
    }
    function Context(t) {
      this.tryEntries = [{
        tryLoc: "root"
      }], t.forEach(pushTryEntry, this), this.reset(!0);
    }
    function values(e) {
      if (e || "" === e) {
        var r = e[a];
        if (r) return r.call(e);
        if ("function" == typeof e.next) return e;
        if (!isNaN(e.length)) {
          var o = -1,
            i = function next() {
              for (; ++o < e.length;) if (n.call(e, o)) return next.value = e[o], next.done = !1, next;
              return next.value = t, next.done = !0, next;
            };
          return i.next = i;
        }
      }
      throw new TypeError($c3b5db471f8fb616d9495023ad42cee1$var$_typeof(e) + " is not iterable");
    }
    return GeneratorFunction.prototype = GeneratorFunctionPrototype, o(g, "constructor", {
      value: GeneratorFunctionPrototype,
      configurable: !0
    }), o(GeneratorFunctionPrototype, "constructor", {
      value: GeneratorFunction,
      configurable: !0
    }), GeneratorFunction.displayName = define(GeneratorFunctionPrototype, u, "GeneratorFunction"), e.isGeneratorFunction = function (t) {
      var e = "function" == typeof t && t.constructor;
      return !!e && (e === GeneratorFunction || "GeneratorFunction" === (e.displayName || e.name));
    }, e.mark = function (t) {
      return Object.setPrototypeOf ? Object.setPrototypeOf(t, GeneratorFunctionPrototype) : (t.__proto__ = GeneratorFunctionPrototype, define(t, u, "GeneratorFunction")), t.prototype = Object.create(g), t;
    }, e.awrap = function (t) {
      return {
        __await: t
      };
    }, defineIteratorMethods(AsyncIterator.prototype), define(AsyncIterator.prototype, c, function () {
      return this;
    }), e.AsyncIterator = AsyncIterator, e.async = function (t, r, n, o, i) {
      void 0 === i && (i = Promise);
      var a = new AsyncIterator(wrap(t, r, n, o), i);
      return e.isGeneratorFunction(r) ? a : a.next().then(function (t) {
        return t.done ? t.value : a.next();
      });
    }, defineIteratorMethods(g), define(g, u, "Generator"), define(g, a, function () {
      return this;
    }), define(g, "toString", function () {
      return "[object Generator]";
    }), e.keys = function (t) {
      var e = Object(t),
        r = [];
      for (var n in e) r.push(n);
      return r.reverse(), function next() {
        for (; r.length;) {
          var t = r.pop();
          if (t in e) return next.value = t, next.done = !1, next;
        }
        return next.done = !0, next;
      };
    }, e.values = values, Context.prototype = {
      constructor: Context,
      reset: function reset(e) {
        if (this.prev = 0, this.next = 0, this.sent = this._sent = t, this.done = !1, this.delegate = null, this.method = "next", this.arg = t, this.tryEntries.forEach(resetTryEntry), !e) for (var r in this) "t" === r.charAt(0) && n.call(this, r) && !isNaN(+r.slice(1)) && (this[r] = t);
      },
      stop: function stop() {
        this.done = !0;
        var t = this.tryEntries[0].completion;
        if ("throw" === t.type) throw t.arg;
        return this.rval;
      },
      dispatchException: function dispatchException(e) {
        if (this.done) throw e;
        var r = this;
        function handle(n, o) {
          return a.type = "throw", a.arg = e, r.next = n, o && (r.method = "next", r.arg = t), !!o;
        }
        for (var o = this.tryEntries.length - 1; o >= 0; --o) {
          var i = this.tryEntries[o],
            a = i.completion;
          if ("root" === i.tryLoc) return handle("end");
          if (i.tryLoc <= this.prev) {
            var c = n.call(i, "catchLoc"),
              u = n.call(i, "finallyLoc");
            if (c && u) {
              if (this.prev < i.catchLoc) return handle(i.catchLoc, !0);
              if (this.prev < i.finallyLoc) return handle(i.finallyLoc);
            } else if (c) {
              if (this.prev < i.catchLoc) return handle(i.catchLoc, !0);
            } else {
              if (!u) throw new Error("try statement without catch or finally");
              if (this.prev < i.finallyLoc) return handle(i.finallyLoc);
            }
          }
        }
      },
      abrupt: function abrupt(t, e) {
        for (var r = this.tryEntries.length - 1; r >= 0; --r) {
          var o = this.tryEntries[r];
          if (o.tryLoc <= this.prev && n.call(o, "finallyLoc") && this.prev < o.finallyLoc) {
            var i = o;
            break;
          }
        }
        i && ("break" === t || "continue" === t) && i.tryLoc <= e && e <= i.finallyLoc && (i = null);
        var a = i ? i.completion : {};
        return a.type = t, a.arg = e, i ? (this.method = "next", this.next = i.finallyLoc, y) : this.complete(a);
      },
      complete: function complete(t, e) {
        if ("throw" === t.type) throw t.arg;
        return "break" === t.type || "continue" === t.type ? this.next = t.arg : "return" === t.type ? (this.rval = this.arg = t.arg, this.method = "return", this.next = "end") : "normal" === t.type && e && (this.next = e), y;
      },
      finish: function finish(t) {
        for (var e = this.tryEntries.length - 1; e >= 0; --e) {
          var r = this.tryEntries[e];
          if (r.finallyLoc === t) return this.complete(r.completion, r.afterLoc), resetTryEntry(r), y;
        }
      },
      catch: function _catch(t) {
        for (var e = this.tryEntries.length - 1; e >= 0; --e) {
          var r = this.tryEntries[e];
          if (r.tryLoc === t) {
            var n = r.completion;
            if ("throw" === n.type) {
              var o = n.arg;
              resetTryEntry(r);
            }
            return o;
          }
        }
        throw new Error("illegal catch attempt");
      },
      delegateYield: function delegateYield(e, r, n) {
        return this.delegate = {
          iterator: values(e),
          resultName: r,
          nextLoc: n
        }, "next" === this.method && (this.arg = t), y;
      }
    }, e;
  }
  function $c3b5db471f8fb616d9495023ad42cee1$var$asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) {
    try {
      var info = gen[key](arg);
      var value = info.value;
    } catch (error) {
      reject(error);
      return;
    }
    if (info.done) {
      resolve(value);
    } else {
      Promise.resolve(value).then(_next, _throw);
    }
  }
  function $c3b5db471f8fb616d9495023ad42cee1$var$_asyncToGenerator(fn) {
    return function () {
      var self = this,
        args = arguments;
      return new Promise(function (resolve, reject) {
        var gen = fn.apply(self, args);
        function _next(value) {
          $c3b5db471f8fb616d9495023ad42cee1$var$asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value);
        }
        function _throw(err) {
          $c3b5db471f8fb616d9495023ad42cee1$var$asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err);
        }
        _next(undefined);
      });
    };
  }
  function $c3b5db471f8fb616d9495023ad42cee1$var$ownKeys(e, r) {
    var t = Object.keys(e);
    if (Object.getOwnPropertySymbols) {
      var o = Object.getOwnPropertySymbols(e);
      r && (o = o.filter(function (r) {
        return Object.getOwnPropertyDescriptor(e, r).enumerable;
      })), t.push.apply(t, o);
    }
    return t;
  }
  function $c3b5db471f8fb616d9495023ad42cee1$var$_objectSpread(e) {
    for (var r = 1; r < arguments.length; r++) {
      var t = null != arguments[r] ? arguments[r] : {};
      r % 2 ? $c3b5db471f8fb616d9495023ad42cee1$var$ownKeys(Object(t), !0).forEach(function (r) {
        $c3b5db471f8fb616d9495023ad42cee1$var$_defineProperty(e, r, t[r]);
      }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(e, Object.getOwnPropertyDescriptors(t)) : $c3b5db471f8fb616d9495023ad42cee1$var$ownKeys(Object(t)).forEach(function (r) {
        Object.defineProperty(e, r, Object.getOwnPropertyDescriptor(t, r));
      });
    }
    return e;
  }
  function $c3b5db471f8fb616d9495023ad42cee1$var$_defineProperty(obj, key, value) {
    key = $c3b5db471f8fb616d9495023ad42cee1$var$_toPropertyKey(key);
    if (key in obj) {
      Object.defineProperty(obj, key, {
        value: value,
        enumerable: true,
        configurable: true,
        writable: true
      });
    } else {
      obj[key] = value;
    }
    return obj;
  }
  function $c3b5db471f8fb616d9495023ad42cee1$var$_toPropertyKey(t) {
    var i = $c3b5db471f8fb616d9495023ad42cee1$var$_toPrimitive(t, "string");
    return "symbol" == $c3b5db471f8fb616d9495023ad42cee1$var$_typeof(i) ? i : String(i);
  }
  function $c3b5db471f8fb616d9495023ad42cee1$var$_toPrimitive(t, r) {
    if ("object" != $c3b5db471f8fb616d9495023ad42cee1$var$_typeof(t) || !t) return t;
    var e = t[Symbol.toPrimitive];
    if (void 0 !== e) {
      var i = e.call(t, r || "default");
      if ("object" != $c3b5db471f8fb616d9495023ad42cee1$var$_typeof(i)) return i;
      throw new TypeError("@@toPrimitive must return a primitive value.");
    }
    return ("string" === r ? String : Number)(t);
  }
  var $c3b5db471f8fb616d9495023ad42cee1$export$state = {
    recipe: {},
    search: {
      query: '',
      result: [],
      page: 1,
      resultPerPage: $ad22e0a18c2bbded425be092f88aba2e$export$RESULT_PER_PAGE
    },
    bookmarks: []
  };
  var $c3b5db471f8fb616d9495023ad42cee1$var$createRecipeObject = function createRecipeObject(data) {
    var recipeData = data.data.recipe;
    return $c3b5db471f8fb616d9495023ad42cee1$var$_objectSpread({
      id: recipeData.id,
      title: recipeData.title,
      publisher: recipeData.publisher,
      sourceUrl: recipeData.source_url,
      image: recipeData.image_url,
      servings: recipeData.servings,
      cookingTime: recipeData.cooking_time,
      ingredients: recipeData.ingredients
    }, recipeData.key && {
      key: recipeData.key
    });
  };
  var $c3b5db471f8fb616d9495023ad42cee1$export$loadRecipe = /*#__PURE__*/function () {
    var _ref = $c3b5db471f8fb616d9495023ad42cee1$var$_asyncToGenerator( /*#__PURE__*/$c3b5db471f8fb616d9495023ad42cee1$var$_regeneratorRuntime().mark(function _callee(id) {
      var data;
      return $c3b5db471f8fb616d9495023ad42cee1$var$_regeneratorRuntime().wrap(function _callee$(_context) {
        while (1) switch (_context.prev = _context.next) {
          case 0:
            _context.prev = 0;
            _context.next = 3;
            return $fe40ff4fe3c4c74bdad83178dfb5e92$export$AJAX("".concat($ad22e0a18c2bbded425be092f88aba2e$export$API_URL).concat(id, "?key=").concat($ad22e0a18c2bbded425be092f88aba2e$export$KEY));
          case 3:
            data = _context.sent;
            $c3b5db471f8fb616d9495023ad42cee1$export$state.recipe = $c3b5db471f8fb616d9495023ad42cee1$var$createRecipeObject(data);
            if ($c3b5db471f8fb616d9495023ad42cee1$export$state.bookmarks.some(function (bookmark) {
              return bookmark.id === id;
            })) $c3b5db471f8fb616d9495023ad42cee1$export$state.recipe.bookmarked = true;else $c3b5db471f8fb616d9495023ad42cee1$export$state.recipe.bookmarked = false;
            _context.next = 11;
            break;
          case 8:
            _context.prev = 8;
            _context.t0 = _context["catch"](0);
            throw _context.t0;
          case 11:
          case "end":
            return _context.stop();
        }
      }, _callee, null, [[0, 8]]);
    }));
    return function loadRecipe(_x) {
      return _ref.apply(this, arguments);
    };
  }();
  var $c3b5db471f8fb616d9495023ad42cee1$export$loadSearchResults = /*#__PURE__*/function () {
    var _ref2 = $c3b5db471f8fb616d9495023ad42cee1$var$_asyncToGenerator( /*#__PURE__*/$c3b5db471f8fb616d9495023ad42cee1$var$_regeneratorRuntime().mark(function _callee2(query) {
      var _data$data, data;
      return $c3b5db471f8fb616d9495023ad42cee1$var$_regeneratorRuntime().wrap(function _callee2$(_context2) {
        while (1) switch (_context2.prev = _context2.next) {
          case 0:
            _context2.prev = 0;
            _context2.next = 3;
            return $fe40ff4fe3c4c74bdad83178dfb5e92$export$AJAX("".concat($ad22e0a18c2bbded425be092f88aba2e$export$API_URL, "?search=").concat(query, "&key=").concat($ad22e0a18c2bbded425be092f88aba2e$export$KEY));
          case 3:
            data = _context2.sent;
            $c3b5db471f8fb616d9495023ad42cee1$export$state.search.query = query;
            $c3b5db471f8fb616d9495023ad42cee1$export$state.search.result = data === null || data === void 0 || (_data$data = data.data) === null || _data$data === void 0 ? void 0 : _data$data.recipes.map(function (recipeData) {
              return $c3b5db471f8fb616d9495023ad42cee1$var$_objectSpread({
                id: recipeData.id,
                title: recipeData.title,
                publisher: recipeData.publisher,
                image: recipeData.image_url
              }, recipeData.key && {
                key: recipeData.key
              });
            });
            $c3b5db471f8fb616d9495023ad42cee1$export$state.search.page = 1;
            _context2.next = 12;
            break;
          case 9:
            _context2.prev = 9;
            _context2.t0 = _context2["catch"](0);
            throw _context2.t0;
          case 12:
          case "end":
            return _context2.stop();
        }
      }, _callee2, null, [[0, 9]]);
    }));
    return function loadSearchResults(_x2) {
      return _ref2.apply(this, arguments);
    };
  }();
  var $c3b5db471f8fb616d9495023ad42cee1$export$getSearchResultsPage = function getSearchResultsPage() {
    var page = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : $c3b5db471f8fb616d9495023ad42cee1$export$state.search.page;
    $c3b5db471f8fb616d9495023ad42cee1$export$state.search.page = page;
    var start = (page - 1) * $c3b5db471f8fb616d9495023ad42cee1$export$state.search.resultPerPage;
    var end = page * $c3b5db471f8fb616d9495023ad42cee1$export$state.search.resultPerPage;
    return $c3b5db471f8fb616d9495023ad42cee1$export$state.search.result.slice(start, end);
  };
  var $c3b5db471f8fb616d9495023ad42cee1$export$updateServings = function updateServings(newServings) {
    $c3b5db471f8fb616d9495023ad42cee1$export$state.recipe.ingredients.forEach(function (ing) {
      ing.quantity = newServings * ing.quantity / $c3b5db471f8fb616d9495023ad42cee1$export$state.recipe.servings;
    });
    $c3b5db471f8fb616d9495023ad42cee1$export$state.recipe.servings = newServings;
  };
  var $c3b5db471f8fb616d9495023ad42cee1$var$presistBookmarks = function presistBookmarks() {
    localStorage.setItem('bookmarks', JSON.stringify($c3b5db471f8fb616d9495023ad42cee1$export$state.bookmarks));
  };
  var $c3b5db471f8fb616d9495023ad42cee1$export$addBookmark = function addBookmark(recipe) {
    // Add boomark

    $c3b5db471f8fb616d9495023ad42cee1$export$state.bookmarks.push(recipe);

    //bookmarked current recipe as true
    // if(recipe.id === state.recipe.id)
    $c3b5db471f8fb616d9495023ad42cee1$export$state.recipe.bookmarked = true;
    $c3b5db471f8fb616d9495023ad42cee1$var$presistBookmarks();
  };
  var $c3b5db471f8fb616d9495023ad42cee1$export$deleteBookmark = function deleteBookmark(id) {
    var index = $c3b5db471f8fb616d9495023ad42cee1$export$state.bookmarks.findIndex(function (ele) {
      return ele.id === id;
    });
    $c3b5db471f8fb616d9495023ad42cee1$export$state.bookmarks.splice(index, 1);

    // if(id === state.recipe.id)
    $c3b5db471f8fb616d9495023ad42cee1$export$state.recipe.bookmarked = false;
    console.log($c3b5db471f8fb616d9495023ad42cee1$export$state.bookmarks);
    $c3b5db471f8fb616d9495023ad42cee1$var$presistBookmarks();
  };
  var $c3b5db471f8fb616d9495023ad42cee1$var$init = function init() {
    var storage = localStorage.getItem('bookmarks');
    if (storage) $c3b5db471f8fb616d9495023ad42cee1$export$state.bookmarks = JSON.parse(storage);
  };
  $c3b5db471f8fb616d9495023ad42cee1$var$init();
  //clearBookmark();
  // console.log(state.bookmarks);

  var $c3b5db471f8fb616d9495023ad42cee1$export$uploadRecipe = /*#__PURE__*/function () {
    var _ref3 = $c3b5db471f8fb616d9495023ad42cee1$var$_asyncToGenerator( /*#__PURE__*/$c3b5db471f8fb616d9495023ad42cee1$var$_regeneratorRuntime().mark(function _callee3(newRecipe) {
      var ingredients, recipe, data;
      return $c3b5db471f8fb616d9495023ad42cee1$var$_regeneratorRuntime().wrap(function _callee3$(_context3) {
        while (1) switch (_context3.prev = _context3.next) {
          case 0:
            _context3.prev = 0;
            ingredients = Object.entries(newRecipe).filter(function (entry) {
              return entry[0].startsWith("ingredient") && entry[1] !== '';
            });
            ingredients = ingredients.map(function (ing) {
              var ingArr = ing[1].replaceAll(' ', '').split(',');
              if (ingArr.length !== 3) throw new Error('Wrong ingredient format! Please use the correct format');
              var _ingArr = $c3b5db471f8fb616d9495023ad42cee1$var$_slicedToArray(ingArr, 3),
                quantity = _ingArr[0],
                unit = _ingArr[1],
                description = _ingArr[2];
              return {
                quantity: quantity ? +quantity : null,
                unit: unit,
                description: description
              };
            });
            recipe = {
              title: newRecipe.title,
              publisher: newRecipe.publisher,
              source_url: newRecipe.sourceUrl,
              image_url: newRecipe.image,
              servings: +newRecipe.servings,
              cooking_time: +newRecipe.cookingTime,
              ingredients: ingredients
            };
            console.log(recipe);
            _context3.next = 7;
            return $fe40ff4fe3c4c74bdad83178dfb5e92$export$AJAX("".concat($ad22e0a18c2bbded425be092f88aba2e$export$API_URL, "?key=").concat($ad22e0a18c2bbded425be092f88aba2e$export$KEY), recipe);
          case 7:
            data = _context3.sent;
            $c3b5db471f8fb616d9495023ad42cee1$export$state.recipe = $c3b5db471f8fb616d9495023ad42cee1$var$createRecipeObject(data);
            $c3b5db471f8fb616d9495023ad42cee1$export$addBookmark($c3b5db471f8fb616d9495023ad42cee1$export$state.recipe);
            _context3.next = 15;
            break;
          case 12:
            _context3.prev = 12;
            _context3.t0 = _context3["catch"](0);
            throw _context3.t0;
          case 15:
          case "end":
            return _context3.stop();
        }
      }, _callee3, null, [[0, 12]]);
    }));
    return function uploadRecipe(_x3) {
      return _ref3.apply(this, arguments);
    };
  }(); // ASSET: /home/himanshu/Downloads/Javascript/complete-javascript-course/18-forkify/starter/src/js/controller.js
  // ASSET: /home/himanshu/Downloads/Javascript/complete-javascript-course/18-forkify/starter/src/js/views/recipeView.js
  // ASSET: /home/himanshu/Downloads/Javascript/complete-javascript-course/18-forkify/starter/node_modules/@parcel/runtime-js/lib/JSRuntime.js
  var $c1749cae2fa147c6f84e7ca474928c37$exports = {}; // ASSET: /home/himanshu/Downloads/Javascript/complete-javascript-course/18-forkify/starter/node_modules/@parcel/runtime-js/lib/bundle-url.js
  /* globals document:readonly */
  var $da3a6c17234c5d68d4f1108f53a7bad4$var$bundleURL = null;
  function $da3a6c17234c5d68d4f1108f53a7bad4$var$getBundleURLCached() {
    if (!$da3a6c17234c5d68d4f1108f53a7bad4$var$bundleURL) {
      $da3a6c17234c5d68d4f1108f53a7bad4$var$bundleURL = $da3a6c17234c5d68d4f1108f53a7bad4$var$getBundleURL();
    }
    return $da3a6c17234c5d68d4f1108f53a7bad4$var$bundleURL;
  }
  function $da3a6c17234c5d68d4f1108f53a7bad4$var$getBundleURL() {
    try {
      throw new Error();
    } catch (err) {
      var matches = ('' + err.stack).match(/(https?|file|ftp):\/\/[^)\n]+/g);
      if (matches) {
        return $da3a6c17234c5d68d4f1108f53a7bad4$var$getBaseURL(matches[0]);
      }
    }
    return '/';
  }
  function $da3a6c17234c5d68d4f1108f53a7bad4$var$getBaseURL(url) {
    return ('' + url).replace(/^((?:https?|file|ftp):\/\/.+)\/[^/]+$/, '$1') + '/';
  } // TODO: Replace uses with `new URL(url).origin` when ie11 is no longer supported.
  var $da3a6c17234c5d68d4f1108f53a7bad4$export$getBundleURL = $da3a6c17234c5d68d4f1108f53a7bad4$var$getBundleURLCached;
  // ASSET: /home/himanshu/Downloads/Javascript/complete-javascript-course/18-forkify/starter/node_modules/@parcel/runtime-js/lib/relative-path.js
  var $e5b69ae2ffddc223b376d75aff9c28$exports = {};
  var $e5b69ae2ffddc223b376d75aff9c28$var$resolve = $bd72ee1865b930c1fed8ae47f35e91$export$resolve;
  $e5b69ae2ffddc223b376d75aff9c28$exports = function (fromId, toId) {
    return $e5b69ae2ffddc223b376d75aff9c28$var$relative($e5b69ae2ffddc223b376d75aff9c28$var$dirname($e5b69ae2ffddc223b376d75aff9c28$var$resolve(fromId)), $e5b69ae2ffddc223b376d75aff9c28$var$resolve(toId));
  };
  function $e5b69ae2ffddc223b376d75aff9c28$var$dirname(_filePath) {
    if (_filePath === '') {
      return '.';
    }
    var filePath = _filePath[_filePath.length - 1] === '/' ? _filePath.slice(0, _filePath.length - 1) : _filePath;
    var slashIndex = filePath.lastIndexOf('/');
    return slashIndex === -1 ? '.' : filePath.slice(0, slashIndex);
  }
  function $e5b69ae2ffddc223b376d75aff9c28$var$relative(from, to) {
    if (from === to) {
      return '';
    }
    var fromParts = from.split('/');
    if (fromParts[0] === '.') {
      fromParts.shift();
    }
    var toParts = to.split('/');
    if (toParts[0] === '.') {
      toParts.shift();
    } // Find where path segments diverge.

    var i;
    var divergeIndex;
    for (i = 0; (i < toParts.length || i < fromParts.length) && divergeIndex == null; i++) {
      if (fromParts[i] !== toParts[i]) {
        divergeIndex = i;
      }
    } // If there are segments from "from" beyond the point of divergence,
    // return back up the path to that point using "..".

    var parts = [];
    for (i = 0; i < fromParts.length - divergeIndex; i++) {
      parts.push('..');
    } // If there are segments from "to" beyond the point of divergence,
    // continue using the remaining segments.

    if (toParts.length > divergeIndex) {
      parts.push.apply(parts, toParts.slice(divergeIndex));
    }
    return parts.join('/');
  }
  var $e5b69ae2ffddc223b376d75aff9c28$export$_dirname = $e5b69ae2ffddc223b376d75aff9c28$var$dirname;
  $e5b69ae2ffddc223b376d75aff9c28$exports._dirname = $e5b69ae2ffddc223b376d75aff9c28$export$_dirname;
  var $e5b69ae2ffddc223b376d75aff9c28$export$_relative = $e5b69ae2ffddc223b376d75aff9c28$var$relative;
  $e5b69ae2ffddc223b376d75aff9c28$exports._relative = $e5b69ae2ffddc223b376d75aff9c28$export$_relative;
  $c1749cae2fa147c6f84e7ca474928c37$exports = $da3a6c17234c5d68d4f1108f53a7bad4$export$getBundleURL() + $e5b69ae2ffddc223b376d75aff9c28$exports("20a8cf28ae6a204b", "3eec49d8191ebba7"); // ASSET: /home/himanshu/Downloads/Javascript/complete-javascript-course/18-forkify/starter/node_modules/fractional/index.js
  /*
  fraction.js
  A Javascript fraction library.
  
  Copyright (c) 2009  Erik Garrison <erik@hypervolu.me>
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  
  */

  /* Fractions */
  /* 
   *
   * Fraction objects are comprised of a numerator and a denomenator.  These
   * values can be accessed at fraction.numerator and fraction.denomenator.
   *
   * Fractions are always returned and stored in lowest-form normalized format.
   * This is accomplished via Fraction.normalize.
   *
   * The following mathematical operations on fractions are supported:
   *
   * Fraction.equals
   * Fraction.add
   * Fraction.subtract
   * Fraction.multiply
   * Fraction.divide
   *
   * These operations accept both numbers and fraction objects.  (Best results
   * are guaranteed when the input is a fraction object.)  They all return a new
   * Fraction object.
   *
   * Usage:
   *
   * TODO
   *
   */

  /*
   * The Fraction constructor takes one of:
   *   an explicit numerator (integer) and denominator (integer),
   *   a string representation of the fraction (string),
   *   or a floating-point number (float)
   *
   * These initialization methods are provided for convenience.  Because of
   * rounding issues the best results will be given when the fraction is
   * constructed from an explicit integer numerator and denomenator, and not a
   * decimal number.
   *
   *
   * e.g. new Fraction(1, 2) --> 1/2
   *      new Fraction('1/2') --> 1/2
   *      new Fraction('2 3/4') --> 11/4  (prints as 2 3/4)
   *
   */
  Fraction = function (numerator, denominator) {
    /* double argument invocation */
    if (typeof numerator !== 'undefined' && denominator) {
      if (typeof numerator === 'number' && typeof denominator === 'number') {
        this.numerator = numerator;
        this.denominator = denominator;
      } else if (typeof numerator === 'string' && typeof denominator === 'string') {
        // what are they?
        // hmm....
        // assume they are ints?
        this.numerator = parseInt(numerator);
        this.denominator = parseInt(denominator);
      }
      /* single-argument invocation */
    } else if (typeof denominator === 'undefined') {
      num = numerator; // swap variable names for legibility
      if (typeof num === 'number') {
        // just a straight number init
        this.numerator = num;
        this.denominator = 1;
      } else if (typeof num === 'string') {
        var a, b; // hold the first and second part of the fraction, e.g. a = '1' and b = '2/3' in 1 2/3
        // or a = '2/3' and b = undefined if we are just passed a single-part number
        var arr = num.split(' ');
        if (arr[0]) a = arr[0];
        if (arr[1]) b = arr[1];
        /* compound fraction e.g. 'A B/C' */
        //  if a is an integer ...
        if (a % 1 === 0 && b && b.match('/')) {
          return new Fraction(a).add(new Fraction(b));
        } else if (a && !b) {
          /* simple fraction e.g. 'A/B' */
          if (typeof a === 'string' && a.match('/')) {
            // it's not a whole number... it's actually a fraction without a whole part written
            var f = a.split('/');
            this.numerator = f[0];
            this.denominator = f[1];
            /* string floating point */
          } else if (typeof a === 'string' && a.match('\.')) {
            return new Fraction(parseFloat(a));
            /* whole number e.g. 'A' */
          } else {
            // just passed a whole number as a string
            this.numerator = parseInt(a);
            this.denominator = 1;
          }
        } else {
          return undefined; // could not parse
        }
      }
    }
    this.normalize();
  };
  Fraction.prototype.clone = function () {
    return new Fraction(this.numerator, this.denominator);
  };

  /* pretty-printer, converts fractions into whole numbers and fractions */
  Fraction.prototype.toString = function () {
    if (this.denominator === 'NaN') return 'NaN';
    var wholepart = this.numerator / this.denominator > 0 ? Math.floor(this.numerator / this.denominator) : Math.ceil(this.numerator / this.denominator);
    var numerator = this.numerator % this.denominator;
    var denominator = this.denominator;
    var result = [];
    if (wholepart != 0) result.push(wholepart);
    if (numerator != 0) result.push((wholepart === 0 ? numerator : Math.abs(numerator)) + '/' + denominator);
    return result.length > 0 ? result.join(' ') : 0;
  };

  /* destructively rescale the fraction by some integral factor */
  Fraction.prototype.rescale = function (factor) {
    this.numerator *= factor;
    this.denominator *= factor;
    return this;
  };
  Fraction.prototype.add = function (b) {
    var a = this.clone();
    if (b instanceof Fraction) {
      b = b.clone();
    } else {
      b = new Fraction(b);
    }
    td = a.denominator;
    a.rescale(b.denominator);
    b.rescale(td);
    a.numerator += b.numerator;
    return a.normalize();
  };
  Fraction.prototype.subtract = function (b) {
    var a = this.clone();
    if (b instanceof Fraction) {
      b = b.clone(); // we scale our argument destructively, so clone
    } else {
      b = new Fraction(b);
    }
    td = a.denominator;
    a.rescale(b.denominator);
    b.rescale(td);
    a.numerator -= b.numerator;
    return a.normalize();
  };
  Fraction.prototype.multiply = function (b) {
    var a = this.clone();
    if (b instanceof Fraction) {
      a.numerator *= b.numerator;
      a.denominator *= b.denominator;
    } else if (typeof b === 'number') {
      a.numerator *= b;
    } else {
      return a.multiply(new Fraction(b));
    }
    return a.normalize();
  };
  Fraction.prototype.divide = function (b) {
    var a = this.clone();
    if (b instanceof Fraction) {
      a.numerator *= b.denominator;
      a.denominator *= b.numerator;
    } else if (typeof b === 'number') {
      a.denominator *= b;
    } else {
      return a.divide(new Fraction(b));
    }
    return a.normalize();
  };
  Fraction.prototype.equals = function (b) {
    if (!(b instanceof Fraction)) {
      b = new Fraction(b);
    }
    // fractions that are equal should have equal normalized forms
    var a = this.clone().normalize();
    var b = b.clone().normalize();
    return a.numerator === b.numerator && a.denominator === b.denominator;
  };

  /* Utility functions */

  /* Destructively normalize the fraction to its smallest representation. 
   * e.g. 4/16 -> 1/4, 14/28 -> 1/2, etc.
   * This is called after all math ops.
   */
  Fraction.prototype.normalize = function () {
    var isFloat = function (n) {
      return typeof n === 'number' && (n > 0 && n % 1 > 0 && n % 1 < 1 || n < 0 && n % -1 < 0 && n % -1 > -1);
    };
    var roundToPlaces = function (n, places) {
      if (!places) {
        return Math.round(n);
      } else {
        var scalar = Math.pow(10, places);
        return Math.round(n * scalar) / scalar;
      }
    };
    return function () {
      // XXX hackish.  Is there a better way to address this issue?
      //
      /* first check if we have decimals, and if we do eliminate them
       * multiply by the 10 ^ number of decimal places in the number
       * round the number to nine decimal places
       * to avoid js floating point funnies
       */
      if (isFloat(this.denominator)) {
        var rounded = roundToPlaces(this.denominator, 9);
        var scaleup = Math.pow(10, rounded.toString().split('.')[1].length);
        this.denominator = Math.round(this.denominator * scaleup); // this !!! should be a whole number
        //this.numerator *= scaleup;
        this.numerator *= scaleup;
      }
      if (isFloat(this.numerator)) {
        var rounded = roundToPlaces(this.numerator, 9);
        var scaleup = Math.pow(10, rounded.toString().split('.')[1].length);
        this.numerator = Math.round(this.numerator * scaleup); // this !!! should be a whole number
        //this.numerator *= scaleup;
        this.denominator *= scaleup;
      }
      var gcf = Fraction.gcf(this.numerator, this.denominator);
      this.numerator /= gcf;
      this.denominator /= gcf;
      if (this.numerator < 0 && this.denominator < 0 || this.numerator > 0 && this.denominator < 0) {
        this.numerator *= -1;
        this.denominator *= -1;
      }
      return this;
    };
  }();

  /* Takes two numbers and returns their greatest common factor.
   */
  Fraction.gcf = function (a, b) {
    var common_factors = [];
    var fa = Fraction.primeFactors(a);
    var fb = Fraction.primeFactors(b);
    // for each factor in fa
    // if it's also in fb
    // put it into the common factors
    fa.forEach(function (factor) {
      var i = fb.indexOf(factor);
      if (i >= 0) {
        common_factors.push(factor);
        fb.splice(i, 1); // remove from fb
      }
    });
    if (common_factors.length === 0) return 1;
    var gcf = function () {
      var r = common_factors[0];
      var i;
      for (i = 1; i < common_factors.length; i++) {
        r = r * common_factors[i];
      }
      return r;
    }();
    return gcf;
  };

  // Adapted from: 
  // http://www.btinternet.com/~se16/js/factor.htm
  Fraction.primeFactors = function (n) {
    var num = Math.abs(n);
    var factors = [];
    var _factor = 2; // first potential prime factor

    while (_factor * _factor <= num)
    // should we keep looking for factors?
    {
      if (num % _factor === 0)
        // this is a factor
        {
          factors.push(_factor); // so keep it
          num = num / _factor; // and divide our search point by it
        } else {
        _factor++; // and increment
      }
    }
    if (num != 1)
      // If there is anything left at the end...
      {
        // ...this must be the last prime factor
        factors.push(num); //    so it too should be recorded
      }
    return factors; // Return the prime factors
  };
  var $e71d1efa458a44db40d822db6cf5$export$Fraction = Fraction; // ASSET: /home/himanshu/Downloads/Javascript/complete-javascript-course/18-forkify/starter/src/js/views/View.js
  function $b85584bbd00b4a0c9b1159737dbdca4b$var$_typeof(o) {
    "@babel/helpers - typeof";

    return $b85584bbd00b4a0c9b1159737dbdca4b$var$_typeof = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (o) {
      return typeof o;
    } : function (o) {
      return o && "function" == typeof Symbol && o.constructor === Symbol && o !== Symbol.prototype ? "symbol" : typeof o;
    }, $b85584bbd00b4a0c9b1159737dbdca4b$var$_typeof(o);
  }
  function $b85584bbd00b4a0c9b1159737dbdca4b$var$_classCallCheck(instance, Constructor) {
    if (!(instance instanceof Constructor)) {
      throw new TypeError("Cannot call a class as a function");
    }
  }
  function $b85584bbd00b4a0c9b1159737dbdca4b$var$_defineProperties(target, props) {
    for (var i = 0; i < props.length; i++) {
      var descriptor = props[i];
      descriptor.enumerable = descriptor.enumerable || false;
      descriptor.configurable = true;
      if ("value" in descriptor) descriptor.writable = true;
      Object.defineProperty(target, $b85584bbd00b4a0c9b1159737dbdca4b$var$_toPropertyKey(descriptor.key), descriptor);
    }
  }
  function $b85584bbd00b4a0c9b1159737dbdca4b$var$_createClass(Constructor, protoProps, staticProps) {
    if (protoProps) $b85584bbd00b4a0c9b1159737dbdca4b$var$_defineProperties(Constructor.prototype, protoProps);
    if (staticProps) $b85584bbd00b4a0c9b1159737dbdca4b$var$_defineProperties(Constructor, staticProps);
    Object.defineProperty(Constructor, "prototype", {
      writable: false
    });
    return Constructor;
  }
  function $b85584bbd00b4a0c9b1159737dbdca4b$var$_defineProperty(obj, key, value) {
    key = $b85584bbd00b4a0c9b1159737dbdca4b$var$_toPropertyKey(key);
    if (key in obj) {
      Object.defineProperty(obj, key, {
        value: value,
        enumerable: true,
        configurable: true,
        writable: true
      });
    } else {
      obj[key] = value;
    }
    return obj;
  }
  function $b85584bbd00b4a0c9b1159737dbdca4b$var$_toPropertyKey(t) {
    var i = $b85584bbd00b4a0c9b1159737dbdca4b$var$_toPrimitive(t, "string");
    return "symbol" == $b85584bbd00b4a0c9b1159737dbdca4b$var$_typeof(i) ? i : String(i);
  }
  function $b85584bbd00b4a0c9b1159737dbdca4b$var$_toPrimitive(t, r) {
    if ("object" != $b85584bbd00b4a0c9b1159737dbdca4b$var$_typeof(t) || !t) return t;
    var e = t[Symbol.toPrimitive];
    if (void 0 !== e) {
      var i = e.call(t, r || "default");
      if ("object" != $b85584bbd00b4a0c9b1159737dbdca4b$var$_typeof(i)) return i;
      throw new TypeError("@@toPrimitive must return a primitive value.");
    }
    return ("string" === r ? String : Number)(t);
  }
  var $c1749cae2fa147c6f84e7ca474928c37$$interop$default = $parcel$interopDefault($c1749cae2fa147c6f84e7ca474928c37$exports);
  var $b85584bbd00b4a0c9b1159737dbdca4b$export$default = /*#__PURE__*/function () {
    function View() {
      $b85584bbd00b4a0c9b1159737dbdca4b$var$_classCallCheck(this, View);
      /**
       * render the received object to the dom
       * @params{Object} data
       * {*} render
       */
      $b85584bbd00b4a0c9b1159737dbdca4b$var$_defineProperty(this, "_data", void 0);
    }
    $b85584bbd00b4a0c9b1159737dbdca4b$var$_createClass(View, [{
      key: "render",
      value: function render(data) {
        var _render = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : true;
        if (!data || Array.isArray(data) && data.length === 0) return this.renderError();
        this._data = data;
        var markup = this._generateMarkup();
        if (!_render) return markup;
        this._clear();
        this._parentElement.insertAdjacentHTML('afterbegin', markup);
      }
    }, {
      key: "_clear",
      value: function _clear() {
        this._parentElement.innerHTML = '';
      }
    }, {
      key: "update",
      value: function update(data) {
        this._data = data;
        var newMarkup = this._generateMarkup();

        // https://medium.com/tech-learnings/web-apis-document-createdocumentfragment-range-createcontextualfragment-d6d7254690e9#id_token=eyJhbGciOiJSUzI1NiIsImtpZCI6IjFmNDBmMGE4ZWYzZDg4MDk3OGRjODJmMjVjM2VjMzE3YzZhNWI3ODEiLCJ0eXAiOiJKV1QifQ.eyJpc3MiOiJodHRwczovL2FjY291bnRzLmdvb2dsZS5jb20iLCJhenAiOiIyMTYyOTYwMzU4MzQtazFrNnFlMDYwczJ0cDJhMmphbTRsamRjbXMwMHN0dGcuYXBwcy5nb29nbGV1c2VyY29udGVudC5jb20iLCJhdWQiOiIyMTYyOTYwMzU4MzQtazFrNnFlMDYwczJ0cDJhMmphbTRsamRjbXMwMHN0dGcuYXBwcy5nb29nbGV1c2VyY29udGVudC5jb20iLCJzdWIiOiIxMDkwODk3NzE3NTE5MzEyMDk4MjAiLCJlbWFpbCI6ImhpbWFuc2h1a2FzaHlhcDExMjJAZ21haWwuY29tIiwiZW1haWxfdmVyaWZpZWQiOnRydWUsIm5iZiI6MTcwNTE1ODIzNiwibmFtZSI6IkhpbWFuc2h1IEthc2h5YXAiLCJwaWN0dXJlIjoiaHR0cHM6Ly9saDMuZ29vZ2xldXNlcmNvbnRlbnQuY29tL2EvQUNnOG9jSm9aa0E5V1JGV2w4N2REbk1SS0MydHdIMEJQMy1scnROM1VFQnYtRFNPbVdJPXM5Ni1jIiwiZ2l2ZW5fbmFtZSI6IkhpbWFuc2h1IiwiZmFtaWx5X25hbWUiOiJLYXNoeWFwIiwibG9jYWxlIjoiZW4iLCJpYXQiOjE3MDUxNTg1MzYsImV4cCI6MTcwNTE2MjEzNiwianRpIjoiZDNjMzZjNmYyYWNhYTYxMWVkMGU4NmRkMGY4ODJiOWMxNzVjODViNSJ9.i2s_mQHoOqy9EtO6PoRIK-VvolWPcb_JVEw4nzJ75DWc8ETZlHTKCkT6q1EiU9KsTUPXCR7SmiL3S5lfHJ1f5_G0SkmFKl7ckCIAmrVx5zIgeVLhvKK_0rtCyomrZ4f2ZtEyBLcAgHBUmezPsVXVhXxvB1H5FFMhon7tntbJbLNkwg7_1tqctPsmDW4U8ZOde-pZCR1XL3jIyDGfQ4FD9qJKt4GdrSYl3UBkJosJfKw6fwVYgEWNeFtRI2Lm3MKBPnee7p2ppbT3A36fZWWNRwug6ic3XfMa-Fb-k2qCpKD1fqudrnpZGdlEINqxZIe-pfxgZ6LaNAc0i7Py7lX_oQ
        // Instead of adding the conents to the dom again, we can actually filter out what was change
        // and only change those in the dom which will make the page reload smooth.
        var newDom = document.createRange().createContextualFragment(newMarkup);
        var newElements = Array.from(newDom.querySelectorAll('*'));
        var currentElement = Array.from(this._parentElement.querySelectorAll('*'));

        //This will only work when we have a text.
        newElements.forEach(function (newEle, index) {
          var _newEle$firstChild;
          var curEle = currentElement[index];
          if (curEle != undefined && !newEle.isEqualNode(curEle) && ((_newEle$firstChild = newEle.firstChild) === null || _newEle$firstChild === void 0 ? void 0 : _newEle$firstChild.nodeValue.trim()) !== '') {
            curEle.textContent = newEle.textContent;
          }
          if (curEle != undefined && !newEle.isEqualNode(curEle))
            // console.log(Array.from(newEle.attributes));
            Array.from(newEle.attributes).forEach(function (attr) {
              curEle.setAttribute(attr.name, attr.value);
            });
        });

        //Updated changed Attributes.
      }
    }, {
      key: "renderSpinner",
      value: function renderSpinner() {
        var markup = "\n          <div class=\"spinner\">\n          <svg>\n            <use href=\"".concat($c1749cae2fa147c6f84e7ca474928c37$$interop$default, "#icon-loader\"></use>\n          </svg>\n          </div>\n       ");
        this._clear();
        this._parentElement.insertAdjacentHTML('afterbegin', markup);
      }
    }, {
      key: "renderError",
      value: function renderError() {
        var message = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : this._errorMessage;
        var markup = "<div class=\"error\">\n        <div>\n          <svg>\n            <use href=\"".concat($c1749cae2fa147c6f84e7ca474928c37$$interop$default, "#icon-alert-triangle\"></use>\n          </svg>\n        </div>\n        <p>").concat(message, "</p>\n      </div>");
        this._clear();
        this._parentElement.insertAdjacentHTML('afterbegin', markup);
      }
    }, {
      key: "renderMessage",
      value: function renderMessage() {
        var message = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : this._message;
        var markup = "<div class=\"message\">\n        <div>\n          <svg>\n            <use href=\"src/img/".concat($c1749cae2fa147c6f84e7ca474928c37$$interop$default, "#icon-smile\"></use>\n          </svg>\n        </div>\n        <p>").concat(message, "</p>\n      </div>");
        this._clear();
        this._parentElement.insertAdjacentHTML('afterbegin', markup);
      }
    }]);
    return View;
  }();
  function $f620f1e41831269f29485288a0a683$var$_typeof(o) {
    "@babel/helpers - typeof";

    return $f620f1e41831269f29485288a0a683$var$_typeof = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (o) {
      return typeof o;
    } : function (o) {
      return o && "function" == typeof Symbol && o.constructor === Symbol && o !== Symbol.prototype ? "symbol" : typeof o;
    }, $f620f1e41831269f29485288a0a683$var$_typeof(o);
  }
  function $f620f1e41831269f29485288a0a683$var$_classCallCheck(instance, Constructor) {
    if (!(instance instanceof Constructor)) {
      throw new TypeError("Cannot call a class as a function");
    }
  }
  function $f620f1e41831269f29485288a0a683$var$_defineProperties(target, props) {
    for (var i = 0; i < props.length; i++) {
      var descriptor = props[i];
      descriptor.enumerable = descriptor.enumerable || false;
      descriptor.configurable = true;
      if ("value" in descriptor) descriptor.writable = true;
      Object.defineProperty(target, $f620f1e41831269f29485288a0a683$var$_toPropertyKey(descriptor.key), descriptor);
    }
  }
  function $f620f1e41831269f29485288a0a683$var$_createClass(Constructor, protoProps, staticProps) {
    if (protoProps) $f620f1e41831269f29485288a0a683$var$_defineProperties(Constructor.prototype, protoProps);
    if (staticProps) $f620f1e41831269f29485288a0a683$var$_defineProperties(Constructor, staticProps);
    Object.defineProperty(Constructor, "prototype", {
      writable: false
    });
    return Constructor;
  }
  function $f620f1e41831269f29485288a0a683$var$_inherits(subClass, superClass) {
    if (typeof superClass !== "function" && superClass !== null) {
      throw new TypeError("Super expression must either be null or a function");
    }
    subClass.prototype = Object.create(superClass && superClass.prototype, {
      constructor: {
        value: subClass,
        writable: true,
        configurable: true
      }
    });
    Object.defineProperty(subClass, "prototype", {
      writable: false
    });
    if (superClass) $f620f1e41831269f29485288a0a683$var$_setPrototypeOf(subClass, superClass);
  }
  function $f620f1e41831269f29485288a0a683$var$_setPrototypeOf(o, p) {
    $f620f1e41831269f29485288a0a683$var$_setPrototypeOf = Object.setPrototypeOf ? Object.setPrototypeOf.bind() : function _setPrototypeOf(o, p) {
      o.__proto__ = p;
      return o;
    };
    return $f620f1e41831269f29485288a0a683$var$_setPrototypeOf(o, p);
  }
  function $f620f1e41831269f29485288a0a683$var$_createSuper(Derived) {
    var hasNativeReflectConstruct = $f620f1e41831269f29485288a0a683$var$_isNativeReflectConstruct();
    return function _createSuperInternal() {
      var Super = $f620f1e41831269f29485288a0a683$var$_getPrototypeOf(Derived),
        result;
      if (hasNativeReflectConstruct) {
        var NewTarget = $f620f1e41831269f29485288a0a683$var$_getPrototypeOf(this).constructor;
        result = Reflect.construct(Super, arguments, NewTarget);
      } else {
        result = Super.apply(this, arguments);
      }
      return $f620f1e41831269f29485288a0a683$var$_possibleConstructorReturn(this, result);
    };
  }
  function $f620f1e41831269f29485288a0a683$var$_possibleConstructorReturn(self, call) {
    if (call && ($f620f1e41831269f29485288a0a683$var$_typeof(call) === "object" || typeof call === "function")) {
      return call;
    } else if (call !== void 0) {
      throw new TypeError("Derived constructors may only return object or undefined");
    }
    return $f620f1e41831269f29485288a0a683$var$_assertThisInitialized(self);
  }
  function $f620f1e41831269f29485288a0a683$var$_assertThisInitialized(self) {
    if (self === void 0) {
      throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
    }
    return self;
  }
  function $f620f1e41831269f29485288a0a683$var$_isNativeReflectConstruct() {
    if (typeof Reflect === "undefined" || !Reflect.construct) return false;
    if (Reflect.construct.sham) return false;
    if (typeof Proxy === "function") return true;
    try {
      Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {}));
      return true;
    } catch (e) {
      return false;
    }
  }
  function $f620f1e41831269f29485288a0a683$var$_getPrototypeOf(o) {
    $f620f1e41831269f29485288a0a683$var$_getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf.bind() : function _getPrototypeOf(o) {
      return o.__proto__ || Object.getPrototypeOf(o);
    };
    return $f620f1e41831269f29485288a0a683$var$_getPrototypeOf(o);
  }
  function $f620f1e41831269f29485288a0a683$var$_defineProperty(obj, key, value) {
    key = $f620f1e41831269f29485288a0a683$var$_toPropertyKey(key);
    if (key in obj) {
      Object.defineProperty(obj, key, {
        value: value,
        enumerable: true,
        configurable: true,
        writable: true
      });
    } else {
      obj[key] = value;
    }
    return obj;
  }
  function $f620f1e41831269f29485288a0a683$var$_toPropertyKey(t) {
    var i = $f620f1e41831269f29485288a0a683$var$_toPrimitive(t, "string");
    return "symbol" == $f620f1e41831269f29485288a0a683$var$_typeof(i) ? i : String(i);
  }
  function $f620f1e41831269f29485288a0a683$var$_toPrimitive(t, r) {
    if ("object" != $f620f1e41831269f29485288a0a683$var$_typeof(t) || !t) return t;
    var e = t[Symbol.toPrimitive];
    if (void 0 !== e) {
      var i = e.call(t, r || "default");
      if ("object" != $f620f1e41831269f29485288a0a683$var$_typeof(i)) return i;
      throw new TypeError("@@toPrimitive must return a primitive value.");
    }
    return ("string" === r ? String : Number)(t);
  }
  var $f620f1e41831269f29485288a0a683$var$RecipeView = /*#__PURE__*/function (_View) {
    $f620f1e41831269f29485288a0a683$var$_inherits(RecipeView, _View);
    var _super = $f620f1e41831269f29485288a0a683$var$_createSuper(RecipeView);
    function RecipeView() {
      var _this;
      $f620f1e41831269f29485288a0a683$var$_classCallCheck(this, RecipeView);
      for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
        args[_key] = arguments[_key];
      }
      _this = _super.call.apply(_super, [this].concat(args));
      $f620f1e41831269f29485288a0a683$var$_defineProperty($f620f1e41831269f29485288a0a683$var$_assertThisInitialized(_this), "_parentElement", document.querySelector('.recipe'));
      $f620f1e41831269f29485288a0a683$var$_defineProperty($f620f1e41831269f29485288a0a683$var$_assertThisInitialized(_this), "_errorMessage", 'We could not find that the recipe, Please try another one!');
      return _this;
    }
    $f620f1e41831269f29485288a0a683$var$_createClass(RecipeView, [{
      key: "addHandlerRender",
      value: function addHandlerRender(handler) {
        ['hashchange', 'load'].forEach(function (evt) {
          return window.addEventListener(evt, handler);
        });
      }
    }, {
      key: "addHandlerUpdateServings",
      value: function addHandlerUpdateServings(handler) {
        this._parentElement.addEventListener('click', function (e) {
          var btn = e.target.closest('.btn--tiny ');
          if (!btn) return;
          handler(btn);
        });
      }
    }, {
      key: "addHandlerAddBookmark",
      value: function addHandlerAddBookmark(handler) {
        this._parentElement.addEventListener('click', function (e) {
          var bookMarkedButton = e.target.closest('.btn--bookmarked');
          if (!bookMarkedButton) return;
          handler();
        });
      }
    }, {
      key: "_generateMarkup",
      value: function _generateMarkup() {
        return "\n        <figure class=\"recipe__fig\">\n        <img src=\"".concat(this._data.image, "\" alt=\"Tomato\" class=\"recipe__img\" />\n        <h1 class=\"recipe__title\">\n            <span>").concat(this._data.title, "</span>\n        </h1>\n        </figure>\n\n        <div class=\"recipe__details\">\n            <div class=\"recipe__info\">\n            <svg class=\"recipe__info-icon\">\n                <use href=\"").concat($c1749cae2fa147c6f84e7ca474928c37$$interop$default, "#icon-clock\"></use>\n            </svg>\n            <span class=\"recipe__info-data recipe__info-data--minutes\">").concat(this._data.cookingTime, "</span>\n            <span class=\"recipe__info-text\">minutes</span>\n            </div>\n            <div class=\"recipe__info\">\n            <svg class=\"recipe__info-icon\">\n                <use href=\"").concat($c1749cae2fa147c6f84e7ca474928c37$$interop$default, "#icon-users\"></use>\n            </svg>\n            <span class=\"recipe__info-data recipe__info-data--people\">").concat(this._data.servings, "</span>\n            <span class=\"recipe__info-text\">servings</span>\n\n            <div class=\"recipe__info-buttons\">\n                <button class=\"btn--tiny btn--increase-servings\" data-update-to=\"").concat(this._data.servings - 1, "\">\n                <svg>\n                    <use href=\"").concat($c1749cae2fa147c6f84e7ca474928c37$$interop$default, "#icon-minus-circle\"></use>\n                </svg>\n                </button>\n                <button class=\"btn--tiny btn--increase-servings\" data-update-to=\"").concat(this._data.servings + 1, "\">\n                <svg>\n                    <use href=\"").concat($c1749cae2fa147c6f84e7ca474928c37$$interop$default, "#icon-plus-circle\"></use>\n                </svg>\n                </button>\n            </div>\n            </div>\n\n            <div class=\"recipe__user-generated ").concat(this._data.key ? '' : 'hidden', "\">\n                <svg>\n                    <use href=\"").concat($c1749cae2fa147c6f84e7ca474928c37$$interop$default, "#icon-user\"></use>\n                </svg>\n            </div>\n            <button class=\"btn--round btn--bookmarked\">\n            <svg class=\"\">\n                <use href=\"").concat($c1749cae2fa147c6f84e7ca474928c37$$interop$default, "#icon-bookmark").concat(this._data.bookmarked ? '-fill' : '', "\"></use>\n            </svg>\n            </button>\n        </div>\n\n        <div class=\"recipe__ingredients\">\n            <h2 class=\"heading--2\">Recipe ingredients</h2>\n            <ul class=\"recipe__ingredient-list\">\n            ").concat(this._data.ingredients.map(this._generateMarkupIngredient).join(''), "\n        </div>\n\n        <div class=\"recipe__directions\">\n            <h2 class=\"heading--2\">How to cook it</h2>\n            <p class=\"recipe__directions-text\">\n            This recipe was carefully designed and tested by\n            <span class=\"recipe__publisher\">").concat(this._data.publisher, "</span>. Please check out\n            directions at their website.\n            </p>\n            <a\n            class=\"btn--small recipe__btn\"\n            href=").concat(this._data.sourceUrl, " \n            target=\"_blank\"\n            >\n            <span>Directions</span>\n            <svg class=\"search__icon\">\n            <use href=\"").concat($c1749cae2fa147c6f84e7ca474928c37$$interop$default, "#icon-arrow-right\"></use>\n            </svg>\n        </a>\n        </div>\n        ");
      }
    }, {
      key: "_generateMarkupIngredient",
      value: function _generateMarkupIngredient(ingredient) {
        return "\n            <li class=\"recipe__ingredient\">\n                <svg class=\"recipe__icon\">\n                    <use href=\"".concat($c1749cae2fa147c6f84e7ca474928c37$$interop$default, "#icon-check\"></use>\n                </svg>\n                <div class=\"recipe__quantity\">").concat(ingredient.quantity ? new $e71d1efa458a44db40d822db6cf5$export$Fraction(ingredient.quantity).toString() : '', "</div> \n                <div class=\"recipe__description\">\n                    <span class=\"recipe__unit\"> ").concat(ingredient.unit, "</span>\n                    ").concat(ingredient.description, "\n                </div>\n            </li>\n            ");
      }
    }]);
    return RecipeView;
  }($b85584bbd00b4a0c9b1159737dbdca4b$export$default);
  var $f620f1e41831269f29485288a0a683$export$default = new $f620f1e41831269f29485288a0a683$var$RecipeView(); // ASSET: /home/himanshu/Downloads/Javascript/complete-javascript-course/18-forkify/starter/src/js/views/resultView.js
  // ASSET: /home/himanshu/Downloads/Javascript/complete-javascript-course/18-forkify/starter/src/js/views/previewView.js
  function $ab03b9085e859c3b9300930009216$var$_typeof(o) {
    "@babel/helpers - typeof";

    return $ab03b9085e859c3b9300930009216$var$_typeof = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (o) {
      return typeof o;
    } : function (o) {
      return o && "function" == typeof Symbol && o.constructor === Symbol && o !== Symbol.prototype ? "symbol" : typeof o;
    }, $ab03b9085e859c3b9300930009216$var$_typeof(o);
  }
  function $ab03b9085e859c3b9300930009216$var$_classCallCheck(instance, Constructor) {
    if (!(instance instanceof Constructor)) {
      throw new TypeError("Cannot call a class as a function");
    }
  }
  function $ab03b9085e859c3b9300930009216$var$_defineProperties(target, props) {
    for (var i = 0; i < props.length; i++) {
      var descriptor = props[i];
      descriptor.enumerable = descriptor.enumerable || false;
      descriptor.configurable = true;
      if ("value" in descriptor) descriptor.writable = true;
      Object.defineProperty(target, $ab03b9085e859c3b9300930009216$var$_toPropertyKey(descriptor.key), descriptor);
    }
  }
  function $ab03b9085e859c3b9300930009216$var$_createClass(Constructor, protoProps, staticProps) {
    if (protoProps) $ab03b9085e859c3b9300930009216$var$_defineProperties(Constructor.prototype, protoProps);
    if (staticProps) $ab03b9085e859c3b9300930009216$var$_defineProperties(Constructor, staticProps);
    Object.defineProperty(Constructor, "prototype", {
      writable: false
    });
    return Constructor;
  }
  function $ab03b9085e859c3b9300930009216$var$_inherits(subClass, superClass) {
    if (typeof superClass !== "function" && superClass !== null) {
      throw new TypeError("Super expression must either be null or a function");
    }
    subClass.prototype = Object.create(superClass && superClass.prototype, {
      constructor: {
        value: subClass,
        writable: true,
        configurable: true
      }
    });
    Object.defineProperty(subClass, "prototype", {
      writable: false
    });
    if (superClass) $ab03b9085e859c3b9300930009216$var$_setPrototypeOf(subClass, superClass);
  }
  function $ab03b9085e859c3b9300930009216$var$_setPrototypeOf(o, p) {
    $ab03b9085e859c3b9300930009216$var$_setPrototypeOf = Object.setPrototypeOf ? Object.setPrototypeOf.bind() : function _setPrototypeOf(o, p) {
      o.__proto__ = p;
      return o;
    };
    return $ab03b9085e859c3b9300930009216$var$_setPrototypeOf(o, p);
  }
  function $ab03b9085e859c3b9300930009216$var$_createSuper(Derived) {
    var hasNativeReflectConstruct = $ab03b9085e859c3b9300930009216$var$_isNativeReflectConstruct();
    return function _createSuperInternal() {
      var Super = $ab03b9085e859c3b9300930009216$var$_getPrototypeOf(Derived),
        result;
      if (hasNativeReflectConstruct) {
        var NewTarget = $ab03b9085e859c3b9300930009216$var$_getPrototypeOf(this).constructor;
        result = Reflect.construct(Super, arguments, NewTarget);
      } else {
        result = Super.apply(this, arguments);
      }
      return $ab03b9085e859c3b9300930009216$var$_possibleConstructorReturn(this, result);
    };
  }
  function $ab03b9085e859c3b9300930009216$var$_possibleConstructorReturn(self, call) {
    if (call && ($ab03b9085e859c3b9300930009216$var$_typeof(call) === "object" || typeof call === "function")) {
      return call;
    } else if (call !== void 0) {
      throw new TypeError("Derived constructors may only return object or undefined");
    }
    return $ab03b9085e859c3b9300930009216$var$_assertThisInitialized(self);
  }
  function $ab03b9085e859c3b9300930009216$var$_assertThisInitialized(self) {
    if (self === void 0) {
      throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
    }
    return self;
  }
  function $ab03b9085e859c3b9300930009216$var$_isNativeReflectConstruct() {
    if (typeof Reflect === "undefined" || !Reflect.construct) return false;
    if (Reflect.construct.sham) return false;
    if (typeof Proxy === "function") return true;
    try {
      Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {}));
      return true;
    } catch (e) {
      return false;
    }
  }
  function $ab03b9085e859c3b9300930009216$var$_getPrototypeOf(o) {
    $ab03b9085e859c3b9300930009216$var$_getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf.bind() : function _getPrototypeOf(o) {
      return o.__proto__ || Object.getPrototypeOf(o);
    };
    return $ab03b9085e859c3b9300930009216$var$_getPrototypeOf(o);
  }
  function $ab03b9085e859c3b9300930009216$var$_defineProperty(obj, key, value) {
    key = $ab03b9085e859c3b9300930009216$var$_toPropertyKey(key);
    if (key in obj) {
      Object.defineProperty(obj, key, {
        value: value,
        enumerable: true,
        configurable: true,
        writable: true
      });
    } else {
      obj[key] = value;
    }
    return obj;
  }
  function $ab03b9085e859c3b9300930009216$var$_toPropertyKey(t) {
    var i = $ab03b9085e859c3b9300930009216$var$_toPrimitive(t, "string");
    return "symbol" == $ab03b9085e859c3b9300930009216$var$_typeof(i) ? i : String(i);
  }
  function $ab03b9085e859c3b9300930009216$var$_toPrimitive(t, r) {
    if ("object" != $ab03b9085e859c3b9300930009216$var$_typeof(t) || !t) return t;
    var e = t[Symbol.toPrimitive];
    if (void 0 !== e) {
      var i = e.call(t, r || "default");
      if ("object" != $ab03b9085e859c3b9300930009216$var$_typeof(i)) return i;
      throw new TypeError("@@toPrimitive must return a primitive value.");
    }
    return ("string" === r ? String : Number)(t);
  }
  var $ab03b9085e859c3b9300930009216$var$PreviewView = /*#__PURE__*/function (_View) {
    $ab03b9085e859c3b9300930009216$var$_inherits(PreviewView, _View);
    var _super = $ab03b9085e859c3b9300930009216$var$_createSuper(PreviewView);
    function PreviewView() {
      var _this;
      $ab03b9085e859c3b9300930009216$var$_classCallCheck(this, PreviewView);
      for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
        args[_key] = arguments[_key];
      }
      _this = _super.call.apply(_super, [this].concat(args));
      $ab03b9085e859c3b9300930009216$var$_defineProperty($ab03b9085e859c3b9300930009216$var$_assertThisInitialized(_this), "_parentElement", '');
      return _this;
    }
    $ab03b9085e859c3b9300930009216$var$_createClass(PreviewView, [{
      key: "_generateMarkup",
      value: function _generateMarkup() {
        var id = window.location.hash.slice(1);
        return "\n          <li class=\"preview\">\n          <a class=\"preview__link ".concat(this._data.id === id ? 'preview__link--active' : '', "\" href=\"#").concat(this._data.id, "\">\n            <figure class=\"preview__fig\">\n              <img src=\"").concat(this._data.image, "\" alt=\"").concat(this._data.title, "\" />        \n            </figure>\n            <div class=\"preview__data\">\n              <h4 class=\"preview__title\">").concat(this._data.title, "</h4>\n              <p class=\"preview__publisher\">").concat(this._data.publisher, "</p>\n            </div>\n            <div class=\"preview__user-generated ").concat(this._data.key ? '' : 'hidden', "\">\n                <svg>\n                    <use href=\"").concat($c1749cae2fa147c6f84e7ca474928c37$$interop$default, "#icon-user\"></use>\n                </svg>\n            </div>\n          </a>\n        </li>\n        ");
      }
    }]);
    return PreviewView;
  }($b85584bbd00b4a0c9b1159737dbdca4b$export$default);
  var $ab03b9085e859c3b9300930009216$export$default = new $ab03b9085e859c3b9300930009216$var$PreviewView();
  function $fc10709d24e4a04b1b69f091ac5fc55$var$_typeof(o) {
    "@babel/helpers - typeof";

    return $fc10709d24e4a04b1b69f091ac5fc55$var$_typeof = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (o) {
      return typeof o;
    } : function (o) {
      return o && "function" == typeof Symbol && o.constructor === Symbol && o !== Symbol.prototype ? "symbol" : typeof o;
    }, $fc10709d24e4a04b1b69f091ac5fc55$var$_typeof(o);
  }
  function $fc10709d24e4a04b1b69f091ac5fc55$var$_classCallCheck(instance, Constructor) {
    if (!(instance instanceof Constructor)) {
      throw new TypeError("Cannot call a class as a function");
    }
  }
  function $fc10709d24e4a04b1b69f091ac5fc55$var$_defineProperties(target, props) {
    for (var i = 0; i < props.length; i++) {
      var descriptor = props[i];
      descriptor.enumerable = descriptor.enumerable || false;
      descriptor.configurable = true;
      if ("value" in descriptor) descriptor.writable = true;
      Object.defineProperty(target, $fc10709d24e4a04b1b69f091ac5fc55$var$_toPropertyKey(descriptor.key), descriptor);
    }
  }
  function $fc10709d24e4a04b1b69f091ac5fc55$var$_createClass(Constructor, protoProps, staticProps) {
    if (protoProps) $fc10709d24e4a04b1b69f091ac5fc55$var$_defineProperties(Constructor.prototype, protoProps);
    if (staticProps) $fc10709d24e4a04b1b69f091ac5fc55$var$_defineProperties(Constructor, staticProps);
    Object.defineProperty(Constructor, "prototype", {
      writable: false
    });
    return Constructor;
  }
  function $fc10709d24e4a04b1b69f091ac5fc55$var$_inherits(subClass, superClass) {
    if (typeof superClass !== "function" && superClass !== null) {
      throw new TypeError("Super expression must either be null or a function");
    }
    subClass.prototype = Object.create(superClass && superClass.prototype, {
      constructor: {
        value: subClass,
        writable: true,
        configurable: true
      }
    });
    Object.defineProperty(subClass, "prototype", {
      writable: false
    });
    if (superClass) $fc10709d24e4a04b1b69f091ac5fc55$var$_setPrototypeOf(subClass, superClass);
  }
  function $fc10709d24e4a04b1b69f091ac5fc55$var$_setPrototypeOf(o, p) {
    $fc10709d24e4a04b1b69f091ac5fc55$var$_setPrototypeOf = Object.setPrototypeOf ? Object.setPrototypeOf.bind() : function _setPrototypeOf(o, p) {
      o.__proto__ = p;
      return o;
    };
    return $fc10709d24e4a04b1b69f091ac5fc55$var$_setPrototypeOf(o, p);
  }
  function $fc10709d24e4a04b1b69f091ac5fc55$var$_createSuper(Derived) {
    var hasNativeReflectConstruct = $fc10709d24e4a04b1b69f091ac5fc55$var$_isNativeReflectConstruct();
    return function _createSuperInternal() {
      var Super = $fc10709d24e4a04b1b69f091ac5fc55$var$_getPrototypeOf(Derived),
        result;
      if (hasNativeReflectConstruct) {
        var NewTarget = $fc10709d24e4a04b1b69f091ac5fc55$var$_getPrototypeOf(this).constructor;
        result = Reflect.construct(Super, arguments, NewTarget);
      } else {
        result = Super.apply(this, arguments);
      }
      return $fc10709d24e4a04b1b69f091ac5fc55$var$_possibleConstructorReturn(this, result);
    };
  }
  function $fc10709d24e4a04b1b69f091ac5fc55$var$_possibleConstructorReturn(self, call) {
    if (call && ($fc10709d24e4a04b1b69f091ac5fc55$var$_typeof(call) === "object" || typeof call === "function")) {
      return call;
    } else if (call !== void 0) {
      throw new TypeError("Derived constructors may only return object or undefined");
    }
    return $fc10709d24e4a04b1b69f091ac5fc55$var$_assertThisInitialized(self);
  }
  function $fc10709d24e4a04b1b69f091ac5fc55$var$_assertThisInitialized(self) {
    if (self === void 0) {
      throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
    }
    return self;
  }
  function $fc10709d24e4a04b1b69f091ac5fc55$var$_isNativeReflectConstruct() {
    if (typeof Reflect === "undefined" || !Reflect.construct) return false;
    if (Reflect.construct.sham) return false;
    if (typeof Proxy === "function") return true;
    try {
      Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {}));
      return true;
    } catch (e) {
      return false;
    }
  }
  function $fc10709d24e4a04b1b69f091ac5fc55$var$_getPrototypeOf(o) {
    $fc10709d24e4a04b1b69f091ac5fc55$var$_getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf.bind() : function _getPrototypeOf(o) {
      return o.__proto__ || Object.getPrototypeOf(o);
    };
    return $fc10709d24e4a04b1b69f091ac5fc55$var$_getPrototypeOf(o);
  }
  function $fc10709d24e4a04b1b69f091ac5fc55$var$_defineProperty(obj, key, value) {
    key = $fc10709d24e4a04b1b69f091ac5fc55$var$_toPropertyKey(key);
    if (key in obj) {
      Object.defineProperty(obj, key, {
        value: value,
        enumerable: true,
        configurable: true,
        writable: true
      });
    } else {
      obj[key] = value;
    }
    return obj;
  }
  function $fc10709d24e4a04b1b69f091ac5fc55$var$_toPropertyKey(t) {
    var i = $fc10709d24e4a04b1b69f091ac5fc55$var$_toPrimitive(t, "string");
    return "symbol" == $fc10709d24e4a04b1b69f091ac5fc55$var$_typeof(i) ? i : String(i);
  }
  function $fc10709d24e4a04b1b69f091ac5fc55$var$_toPrimitive(t, r) {
    if ("object" != $fc10709d24e4a04b1b69f091ac5fc55$var$_typeof(t) || !t) return t;
    var e = t[Symbol.toPrimitive];
    if (void 0 !== e) {
      var i = e.call(t, r || "default");
      if ("object" != $fc10709d24e4a04b1b69f091ac5fc55$var$_typeof(i)) return i;
      throw new TypeError("@@toPrimitive must return a primitive value.");
    }
    return ("string" === r ? String : Number)(t);
  }
  var $fc10709d24e4a04b1b69f091ac5fc55$var$ResultView = /*#__PURE__*/function (_View) {
    $fc10709d24e4a04b1b69f091ac5fc55$var$_inherits(ResultView, _View);
    var _super = $fc10709d24e4a04b1b69f091ac5fc55$var$_createSuper(ResultView);
    function ResultView() {
      var _this;
      $fc10709d24e4a04b1b69f091ac5fc55$var$_classCallCheck(this, ResultView);
      for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
        args[_key] = arguments[_key];
      }
      _this = _super.call.apply(_super, [this].concat(args));
      $fc10709d24e4a04b1b69f091ac5fc55$var$_defineProperty($fc10709d24e4a04b1b69f091ac5fc55$var$_assertThisInitialized(_this), "_parentElement", document.querySelector('.results'));
      $fc10709d24e4a04b1b69f091ac5fc55$var$_defineProperty($fc10709d24e4a04b1b69f091ac5fc55$var$_assertThisInitialized(_this), "_errorMessage", 'No recipes found for the given query!');
      return _this;
    }
    $fc10709d24e4a04b1b69f091ac5fc55$var$_createClass(ResultView, [{
      key: "_generateMarkup",
      value: function _generateMarkup() {
        return this._data.map(function (result) {
          return $ab03b9085e859c3b9300930009216$export$default.render(result, false);
        }).join('');
      }
    }]);
    return ResultView;
  }($b85584bbd00b4a0c9b1159737dbdca4b$export$default);
  var $fc10709d24e4a04b1b69f091ac5fc55$export$default = new $fc10709d24e4a04b1b69f091ac5fc55$var$ResultView(); // ASSET: /home/himanshu/Downloads/Javascript/complete-javascript-course/18-forkify/starter/src/js/views/searchView.js
  function $f91c83782128716ccea27a6a$var$_typeof(o) {
    "@babel/helpers - typeof";

    return $f91c83782128716ccea27a6a$var$_typeof = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (o) {
      return typeof o;
    } : function (o) {
      return o && "function" == typeof Symbol && o.constructor === Symbol && o !== Symbol.prototype ? "symbol" : typeof o;
    }, $f91c83782128716ccea27a6a$var$_typeof(o);
  }
  function $f91c83782128716ccea27a6a$var$_classCallCheck(instance, Constructor) {
    if (!(instance instanceof Constructor)) {
      throw new TypeError("Cannot call a class as a function");
    }
  }
  function $f91c83782128716ccea27a6a$var$_defineProperties(target, props) {
    for (var i = 0; i < props.length; i++) {
      var descriptor = props[i];
      descriptor.enumerable = descriptor.enumerable || false;
      descriptor.configurable = true;
      if ("value" in descriptor) descriptor.writable = true;
      Object.defineProperty(target, $f91c83782128716ccea27a6a$var$_toPropertyKey(descriptor.key), descriptor);
    }
  }
  function $f91c83782128716ccea27a6a$var$_createClass(Constructor, protoProps, staticProps) {
    if (protoProps) $f91c83782128716ccea27a6a$var$_defineProperties(Constructor.prototype, protoProps);
    if (staticProps) $f91c83782128716ccea27a6a$var$_defineProperties(Constructor, staticProps);
    Object.defineProperty(Constructor, "prototype", {
      writable: false
    });
    return Constructor;
  }
  function $f91c83782128716ccea27a6a$var$_toPropertyKey(t) {
    var i = $f91c83782128716ccea27a6a$var$_toPrimitive(t, "string");
    return "symbol" == $f91c83782128716ccea27a6a$var$_typeof(i) ? i : String(i);
  }
  function $f91c83782128716ccea27a6a$var$_toPrimitive(t, r) {
    if ("object" != $f91c83782128716ccea27a6a$var$_typeof(t) || !t) return t;
    var e = t[Symbol.toPrimitive];
    if (void 0 !== e) {
      var i = e.call(t, r || "default");
      if ("object" != $f91c83782128716ccea27a6a$var$_typeof(i)) return i;
      throw new TypeError("@@toPrimitive must return a primitive value.");
    }
    return ("string" === r ? String : Number)(t);
  }
  function $f91c83782128716ccea27a6a$var$_classPrivateMethodInitSpec(obj, privateSet) {
    $f91c83782128716ccea27a6a$var$_checkPrivateRedeclaration(obj, privateSet);
    privateSet.add(obj);
  }
  function $f91c83782128716ccea27a6a$var$_classPrivateFieldInitSpec(obj, privateMap, value) {
    $f91c83782128716ccea27a6a$var$_checkPrivateRedeclaration(obj, privateMap);
    privateMap.set(obj, value);
  }
  function $f91c83782128716ccea27a6a$var$_checkPrivateRedeclaration(obj, privateCollection) {
    if (privateCollection.has(obj)) {
      throw new TypeError("Cannot initialize the same private elements twice on an object");
    }
  }
  function $f91c83782128716ccea27a6a$var$_classPrivateMethodGet(receiver, privateSet, fn) {
    if (!privateSet.has(receiver)) {
      throw new TypeError("attempted to get private field on non-instance");
    }
    return fn;
  }
  function $f91c83782128716ccea27a6a$var$_classPrivateFieldGet(receiver, privateMap) {
    var descriptor = $f91c83782128716ccea27a6a$var$_classExtractFieldDescriptor(receiver, privateMap, "get");
    return $f91c83782128716ccea27a6a$var$_classApplyDescriptorGet(receiver, descriptor);
  }
  function $f91c83782128716ccea27a6a$var$_classExtractFieldDescriptor(receiver, privateMap, action) {
    if (!privateMap.has(receiver)) {
      throw new TypeError("attempted to " + action + " private field on non-instance");
    }
    return privateMap.get(receiver);
  }
  function $f91c83782128716ccea27a6a$var$_classApplyDescriptorGet(receiver, descriptor) {
    if (descriptor.get) {
      return descriptor.get.call(receiver);
    }
    return descriptor.value;
  }
  var $f91c83782128716ccea27a6a$var$_parentEl = /*#__PURE__*/new WeakMap();
  var $f91c83782128716ccea27a6a$var$_clearInput = /*#__PURE__*/new WeakSet();
  var $f91c83782128716ccea27a6a$var$searchView = /*#__PURE__*/function () {
    function searchView() {
      $f91c83782128716ccea27a6a$var$_classCallCheck(this, searchView);
      $f91c83782128716ccea27a6a$var$_classPrivateMethodInitSpec(this, $f91c83782128716ccea27a6a$var$_clearInput);
      $f91c83782128716ccea27a6a$var$_classPrivateFieldInitSpec(this, $f91c83782128716ccea27a6a$var$_parentEl, {
        writable: true,
        value: document.querySelector('.search')
      });
    }
    $f91c83782128716ccea27a6a$var$_createClass(searchView, [{
      key: "getQuery",
      value: function getQuery() {
        var query = $f91c83782128716ccea27a6a$var$_classPrivateFieldGet(this, $f91c83782128716ccea27a6a$var$_parentEl).querySelector('.search__field').value;
        $f91c83782128716ccea27a6a$var$_classPrivateMethodGet(this, $f91c83782128716ccea27a6a$var$_clearInput, $f91c83782128716ccea27a6a$var$_clearInput2).call(this);
        return query;
      }
    }, {
      key: "addHandlerSearch",
      value: function addHandlerSearch(handler) {
        $f91c83782128716ccea27a6a$var$_classPrivateFieldGet(this, $f91c83782128716ccea27a6a$var$_parentEl).addEventListener('submit', function (e) {
          e.preventDefault();
          handler();
        });
      }
    }]);
    return searchView;
  }();
  function $f91c83782128716ccea27a6a$var$_clearInput2() {
    $f91c83782128716ccea27a6a$var$_classPrivateFieldGet(this, $f91c83782128716ccea27a6a$var$_parentEl).querySelector('.search__field').value = '';
  }
  var $f91c83782128716ccea27a6a$export$default = new $f91c83782128716ccea27a6a$var$searchView(); // ASSET: /home/himanshu/Downloads/Javascript/complete-javascript-course/18-forkify/starter/src/js/views/PaginationView.js
  function $b363fc27ed313e62ddd6f8021836b71$var$_typeof(o) {
    "@babel/helpers - typeof";

    return $b363fc27ed313e62ddd6f8021836b71$var$_typeof = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (o) {
      return typeof o;
    } : function (o) {
      return o && "function" == typeof Symbol && o.constructor === Symbol && o !== Symbol.prototype ? "symbol" : typeof o;
    }, $b363fc27ed313e62ddd6f8021836b71$var$_typeof(o);
  }
  function $b363fc27ed313e62ddd6f8021836b71$var$_classCallCheck(instance, Constructor) {
    if (!(instance instanceof Constructor)) {
      throw new TypeError("Cannot call a class as a function");
    }
  }
  function $b363fc27ed313e62ddd6f8021836b71$var$_defineProperties(target, props) {
    for (var i = 0; i < props.length; i++) {
      var descriptor = props[i];
      descriptor.enumerable = descriptor.enumerable || false;
      descriptor.configurable = true;
      if ("value" in descriptor) descriptor.writable = true;
      Object.defineProperty(target, $b363fc27ed313e62ddd6f8021836b71$var$_toPropertyKey(descriptor.key), descriptor);
    }
  }
  function $b363fc27ed313e62ddd6f8021836b71$var$_createClass(Constructor, protoProps, staticProps) {
    if (protoProps) $b363fc27ed313e62ddd6f8021836b71$var$_defineProperties(Constructor.prototype, protoProps);
    if (staticProps) $b363fc27ed313e62ddd6f8021836b71$var$_defineProperties(Constructor, staticProps);
    Object.defineProperty(Constructor, "prototype", {
      writable: false
    });
    return Constructor;
  }
  function $b363fc27ed313e62ddd6f8021836b71$var$_inherits(subClass, superClass) {
    if (typeof superClass !== "function" && superClass !== null) {
      throw new TypeError("Super expression must either be null or a function");
    }
    subClass.prototype = Object.create(superClass && superClass.prototype, {
      constructor: {
        value: subClass,
        writable: true,
        configurable: true
      }
    });
    Object.defineProperty(subClass, "prototype", {
      writable: false
    });
    if (superClass) $b363fc27ed313e62ddd6f8021836b71$var$_setPrototypeOf(subClass, superClass);
  }
  function $b363fc27ed313e62ddd6f8021836b71$var$_setPrototypeOf(o, p) {
    $b363fc27ed313e62ddd6f8021836b71$var$_setPrototypeOf = Object.setPrototypeOf ? Object.setPrototypeOf.bind() : function _setPrototypeOf(o, p) {
      o.__proto__ = p;
      return o;
    };
    return $b363fc27ed313e62ddd6f8021836b71$var$_setPrototypeOf(o, p);
  }
  function $b363fc27ed313e62ddd6f8021836b71$var$_createSuper(Derived) {
    var hasNativeReflectConstruct = $b363fc27ed313e62ddd6f8021836b71$var$_isNativeReflectConstruct();
    return function _createSuperInternal() {
      var Super = $b363fc27ed313e62ddd6f8021836b71$var$_getPrototypeOf(Derived),
        result;
      if (hasNativeReflectConstruct) {
        var NewTarget = $b363fc27ed313e62ddd6f8021836b71$var$_getPrototypeOf(this).constructor;
        result = Reflect.construct(Super, arguments, NewTarget);
      } else {
        result = Super.apply(this, arguments);
      }
      return $b363fc27ed313e62ddd6f8021836b71$var$_possibleConstructorReturn(this, result);
    };
  }
  function $b363fc27ed313e62ddd6f8021836b71$var$_possibleConstructorReturn(self, call) {
    if (call && ($b363fc27ed313e62ddd6f8021836b71$var$_typeof(call) === "object" || typeof call === "function")) {
      return call;
    } else if (call !== void 0) {
      throw new TypeError("Derived constructors may only return object or undefined");
    }
    return $b363fc27ed313e62ddd6f8021836b71$var$_assertThisInitialized(self);
  }
  function $b363fc27ed313e62ddd6f8021836b71$var$_assertThisInitialized(self) {
    if (self === void 0) {
      throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
    }
    return self;
  }
  function $b363fc27ed313e62ddd6f8021836b71$var$_isNativeReflectConstruct() {
    if (typeof Reflect === "undefined" || !Reflect.construct) return false;
    if (Reflect.construct.sham) return false;
    if (typeof Proxy === "function") return true;
    try {
      Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {}));
      return true;
    } catch (e) {
      return false;
    }
  }
  function $b363fc27ed313e62ddd6f8021836b71$var$_getPrototypeOf(o) {
    $b363fc27ed313e62ddd6f8021836b71$var$_getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf.bind() : function _getPrototypeOf(o) {
      return o.__proto__ || Object.getPrototypeOf(o);
    };
    return $b363fc27ed313e62ddd6f8021836b71$var$_getPrototypeOf(o);
  }
  function $b363fc27ed313e62ddd6f8021836b71$var$_defineProperty(obj, key, value) {
    key = $b363fc27ed313e62ddd6f8021836b71$var$_toPropertyKey(key);
    if (key in obj) {
      Object.defineProperty(obj, key, {
        value: value,
        enumerable: true,
        configurable: true,
        writable: true
      });
    } else {
      obj[key] = value;
    }
    return obj;
  }
  function $b363fc27ed313e62ddd6f8021836b71$var$_toPropertyKey(t) {
    var i = $b363fc27ed313e62ddd6f8021836b71$var$_toPrimitive(t, "string");
    return "symbol" == $b363fc27ed313e62ddd6f8021836b71$var$_typeof(i) ? i : String(i);
  }
  function $b363fc27ed313e62ddd6f8021836b71$var$_toPrimitive(t, r) {
    if ("object" != $b363fc27ed313e62ddd6f8021836b71$var$_typeof(t) || !t) return t;
    var e = t[Symbol.toPrimitive];
    if (void 0 !== e) {
      var i = e.call(t, r || "default");
      if ("object" != $b363fc27ed313e62ddd6f8021836b71$var$_typeof(i)) return i;
      throw new TypeError("@@toPrimitive must return a primitive value.");
    }
    return ("string" === r ? String : Number)(t);
  }
  var $b363fc27ed313e62ddd6f8021836b71$var$PaginationView = /*#__PURE__*/function (_View) {
    $b363fc27ed313e62ddd6f8021836b71$var$_inherits(PaginationView, _View);
    var _super = $b363fc27ed313e62ddd6f8021836b71$var$_createSuper(PaginationView);
    function PaginationView() {
      var _this;
      $b363fc27ed313e62ddd6f8021836b71$var$_classCallCheck(this, PaginationView);
      for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
        args[_key] = arguments[_key];
      }
      _this = _super.call.apply(_super, [this].concat(args));
      $b363fc27ed313e62ddd6f8021836b71$var$_defineProperty($b363fc27ed313e62ddd6f8021836b71$var$_assertThisInitialized(_this), "_parentElement", document.querySelector('.pagination'));
      return _this;
    }
    $b363fc27ed313e62ddd6f8021836b71$var$_createClass(PaginationView, [{
      key: "addHandlerClick",
      value: function addHandlerClick(handler) {
        this._parentElement.addEventListener('click', function (e) {
          var btn = e.target.closest('.btn--inline ');
          if (!btn) return;
          handler(btn);
        });
      }
    }, {
      key: "_generateMarkup",
      value: function _generateMarkup() {
        var numPages = Math.ceil(this._data.result.length / this._data.resultPerPage);
        console.log(numPages);
        var currentPage = this._data.page;
        if (currentPage === 1 && numPages > 1) {
          return this._generateMarkupForwordButton(currentPage);
        } else if (currentPage === numPages && numPages > 1) {
          return this._generateMarkupPreviousButton(currentPage);
        }
        if (currentPage < numPages) {
          return this._generateMarkupPreviousButton(currentPage).concat(this._generateMarkupForwordButton(currentPage));
        }
      }
    }, {
      key: "_generateMarkupPreviousButton",
      value: function _generateMarkupPreviousButton(currentPage) {
        return " \n        <button data-goto= \"".concat(currentPage - 1, "\"class=\"btn--inline pagination__btn--prev\">\n            <svg class=\"search__icon\">\n            <use href=\"").concat($c1749cae2fa147c6f84e7ca474928c37$$interop$default, "#icon-arrow-left\"></use>\n            </svg>\n            <span>Page ").concat(currentPage - 1, "</span>\n        </button>\n            ");
      }
    }, {
      key: "_generateMarkupForwordButton",
      value: function _generateMarkupForwordButton(currentPage) {
        return " \n        <button data-goto= \"".concat(currentPage + 1, "\" class=\"btn--inline pagination__btn--next\">\n            <span>Page ").concat(currentPage + 1, "</span>\n            <svg class=\"search__icon\">\n            <use href=\"").concat($c1749cae2fa147c6f84e7ca474928c37$$interop$default, "#icon-arrow-right\"></use>\n            </svg>\n        </button>\n            ");
      }
    }]);
    return PaginationView;
  }($b85584bbd00b4a0c9b1159737dbdca4b$export$default);
  var $b363fc27ed313e62ddd6f8021836b71$export$default = new $b363fc27ed313e62ddd6f8021836b71$var$PaginationView(); // ASSET: /home/himanshu/Downloads/Javascript/complete-javascript-course/18-forkify/starter/src/js/views/bookMarkedView.js
  function $f29b5555c6852e3bbcd9784393e0c58a$var$_typeof(o) {
    "@babel/helpers - typeof";

    return $f29b5555c6852e3bbcd9784393e0c58a$var$_typeof = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (o) {
      return typeof o;
    } : function (o) {
      return o && "function" == typeof Symbol && o.constructor === Symbol && o !== Symbol.prototype ? "symbol" : typeof o;
    }, $f29b5555c6852e3bbcd9784393e0c58a$var$_typeof(o);
  }
  function $f29b5555c6852e3bbcd9784393e0c58a$var$_classCallCheck(instance, Constructor) {
    if (!(instance instanceof Constructor)) {
      throw new TypeError("Cannot call a class as a function");
    }
  }
  function $f29b5555c6852e3bbcd9784393e0c58a$var$_defineProperties(target, props) {
    for (var i = 0; i < props.length; i++) {
      var descriptor = props[i];
      descriptor.enumerable = descriptor.enumerable || false;
      descriptor.configurable = true;
      if ("value" in descriptor) descriptor.writable = true;
      Object.defineProperty(target, $f29b5555c6852e3bbcd9784393e0c58a$var$_toPropertyKey(descriptor.key), descriptor);
    }
  }
  function $f29b5555c6852e3bbcd9784393e0c58a$var$_createClass(Constructor, protoProps, staticProps) {
    if (protoProps) $f29b5555c6852e3bbcd9784393e0c58a$var$_defineProperties(Constructor.prototype, protoProps);
    if (staticProps) $f29b5555c6852e3bbcd9784393e0c58a$var$_defineProperties(Constructor, staticProps);
    Object.defineProperty(Constructor, "prototype", {
      writable: false
    });
    return Constructor;
  }
  function $f29b5555c6852e3bbcd9784393e0c58a$var$_inherits(subClass, superClass) {
    if (typeof superClass !== "function" && superClass !== null) {
      throw new TypeError("Super expression must either be null or a function");
    }
    subClass.prototype = Object.create(superClass && superClass.prototype, {
      constructor: {
        value: subClass,
        writable: true,
        configurable: true
      }
    });
    Object.defineProperty(subClass, "prototype", {
      writable: false
    });
    if (superClass) $f29b5555c6852e3bbcd9784393e0c58a$var$_setPrototypeOf(subClass, superClass);
  }
  function $f29b5555c6852e3bbcd9784393e0c58a$var$_setPrototypeOf(o, p) {
    $f29b5555c6852e3bbcd9784393e0c58a$var$_setPrototypeOf = Object.setPrototypeOf ? Object.setPrototypeOf.bind() : function _setPrototypeOf(o, p) {
      o.__proto__ = p;
      return o;
    };
    return $f29b5555c6852e3bbcd9784393e0c58a$var$_setPrototypeOf(o, p);
  }
  function $f29b5555c6852e3bbcd9784393e0c58a$var$_createSuper(Derived) {
    var hasNativeReflectConstruct = $f29b5555c6852e3bbcd9784393e0c58a$var$_isNativeReflectConstruct();
    return function _createSuperInternal() {
      var Super = $f29b5555c6852e3bbcd9784393e0c58a$var$_getPrototypeOf(Derived),
        result;
      if (hasNativeReflectConstruct) {
        var NewTarget = $f29b5555c6852e3bbcd9784393e0c58a$var$_getPrototypeOf(this).constructor;
        result = Reflect.construct(Super, arguments, NewTarget);
      } else {
        result = Super.apply(this, arguments);
      }
      return $f29b5555c6852e3bbcd9784393e0c58a$var$_possibleConstructorReturn(this, result);
    };
  }
  function $f29b5555c6852e3bbcd9784393e0c58a$var$_possibleConstructorReturn(self, call) {
    if (call && ($f29b5555c6852e3bbcd9784393e0c58a$var$_typeof(call) === "object" || typeof call === "function")) {
      return call;
    } else if (call !== void 0) {
      throw new TypeError("Derived constructors may only return object or undefined");
    }
    return $f29b5555c6852e3bbcd9784393e0c58a$var$_assertThisInitialized(self);
  }
  function $f29b5555c6852e3bbcd9784393e0c58a$var$_assertThisInitialized(self) {
    if (self === void 0) {
      throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
    }
    return self;
  }
  function $f29b5555c6852e3bbcd9784393e0c58a$var$_isNativeReflectConstruct() {
    if (typeof Reflect === "undefined" || !Reflect.construct) return false;
    if (Reflect.construct.sham) return false;
    if (typeof Proxy === "function") return true;
    try {
      Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {}));
      return true;
    } catch (e) {
      return false;
    }
  }
  function $f29b5555c6852e3bbcd9784393e0c58a$var$_getPrototypeOf(o) {
    $f29b5555c6852e3bbcd9784393e0c58a$var$_getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf.bind() : function _getPrototypeOf(o) {
      return o.__proto__ || Object.getPrototypeOf(o);
    };
    return $f29b5555c6852e3bbcd9784393e0c58a$var$_getPrototypeOf(o);
  }
  function $f29b5555c6852e3bbcd9784393e0c58a$var$_defineProperty(obj, key, value) {
    key = $f29b5555c6852e3bbcd9784393e0c58a$var$_toPropertyKey(key);
    if (key in obj) {
      Object.defineProperty(obj, key, {
        value: value,
        enumerable: true,
        configurable: true,
        writable: true
      });
    } else {
      obj[key] = value;
    }
    return obj;
  }
  function $f29b5555c6852e3bbcd9784393e0c58a$var$_toPropertyKey(t) {
    var i = $f29b5555c6852e3bbcd9784393e0c58a$var$_toPrimitive(t, "string");
    return "symbol" == $f29b5555c6852e3bbcd9784393e0c58a$var$_typeof(i) ? i : String(i);
  }
  function $f29b5555c6852e3bbcd9784393e0c58a$var$_toPrimitive(t, r) {
    if ("object" != $f29b5555c6852e3bbcd9784393e0c58a$var$_typeof(t) || !t) return t;
    var e = t[Symbol.toPrimitive];
    if (void 0 !== e) {
      var i = e.call(t, r || "default");
      if ("object" != $f29b5555c6852e3bbcd9784393e0c58a$var$_typeof(i)) return i;
      throw new TypeError("@@toPrimitive must return a primitive value.");
    }
    return ("string" === r ? String : Number)(t);
  }
  var $f29b5555c6852e3bbcd9784393e0c58a$var$BookmarksView = /*#__PURE__*/function (_View) {
    $f29b5555c6852e3bbcd9784393e0c58a$var$_inherits(BookmarksView, _View);
    var _super = $f29b5555c6852e3bbcd9784393e0c58a$var$_createSuper(BookmarksView);
    function BookmarksView() {
      var _this;
      $f29b5555c6852e3bbcd9784393e0c58a$var$_classCallCheck(this, BookmarksView);
      for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
        args[_key] = arguments[_key];
      }
      _this = _super.call.apply(_super, [this].concat(args));
      $f29b5555c6852e3bbcd9784393e0c58a$var$_defineProperty($f29b5555c6852e3bbcd9784393e0c58a$var$_assertThisInitialized(_this), "_parentElement", document.querySelector('.bookmarks__list'));
      $f29b5555c6852e3bbcd9784393e0c58a$var$_defineProperty($f29b5555c6852e3bbcd9784393e0c58a$var$_assertThisInitialized(_this), "_errorMessage", 'No bookmarks yet. Find a nice recipe and bookmarked it!');
      return _this;
    }
    $f29b5555c6852e3bbcd9784393e0c58a$var$_createClass(BookmarksView, [{
      key: "addHandlerRender",
      value: function addHandlerRender(handler) {
        window.addEventListener('load', handler);
      }
    }, {
      key: "_generateMarkup",
      value: function _generateMarkup() {
        return this._data.map(function (bookmark) {
          return $ab03b9085e859c3b9300930009216$export$default.render(bookmark, false);
        }).join('');
      }
    }]);
    return BookmarksView;
  }($b85584bbd00b4a0c9b1159737dbdca4b$export$default);
  var $f29b5555c6852e3bbcd9784393e0c58a$export$default = new $f29b5555c6852e3bbcd9784393e0c58a$var$BookmarksView(); // ASSET: /home/himanshu/Downloads/Javascript/complete-javascript-course/18-forkify/starter/src/js/views/addRecipeView.js
  function $b4bb92ee48aa94b96ce93e9eb66358$var$_typeof(o) {
    "@babel/helpers - typeof";

    return $b4bb92ee48aa94b96ce93e9eb66358$var$_typeof = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (o) {
      return typeof o;
    } : function (o) {
      return o && "function" == typeof Symbol && o.constructor === Symbol && o !== Symbol.prototype ? "symbol" : typeof o;
    }, $b4bb92ee48aa94b96ce93e9eb66358$var$_typeof(o);
  }
  function $b4bb92ee48aa94b96ce93e9eb66358$var$_toConsumableArray(arr) {
    return $b4bb92ee48aa94b96ce93e9eb66358$var$_arrayWithoutHoles(arr) || $b4bb92ee48aa94b96ce93e9eb66358$var$_iterableToArray(arr) || $b4bb92ee48aa94b96ce93e9eb66358$var$_unsupportedIterableToArray(arr) || $b4bb92ee48aa94b96ce93e9eb66358$var$_nonIterableSpread();
  }
  function $b4bb92ee48aa94b96ce93e9eb66358$var$_nonIterableSpread() {
    throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method.");
  }
  function $b4bb92ee48aa94b96ce93e9eb66358$var$_unsupportedIterableToArray(o, minLen) {
    if (!o) return;
    if (typeof o === "string") return $b4bb92ee48aa94b96ce93e9eb66358$var$_arrayLikeToArray(o, minLen);
    var n = Object.prototype.toString.call(o).slice(8, -1);
    if (n === "Object" && o.constructor) n = o.constructor.name;
    if (n === "Map" || n === "Set") return Array.from(o);
    if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return $b4bb92ee48aa94b96ce93e9eb66358$var$_arrayLikeToArray(o, minLen);
  }
  function $b4bb92ee48aa94b96ce93e9eb66358$var$_iterableToArray(iter) {
    if (typeof Symbol !== "undefined" && iter[Symbol.iterator] != null || iter["@@iterator"] != null) return Array.from(iter);
  }
  function $b4bb92ee48aa94b96ce93e9eb66358$var$_arrayWithoutHoles(arr) {
    if (Array.isArray(arr)) return $b4bb92ee48aa94b96ce93e9eb66358$var$_arrayLikeToArray(arr);
  }
  function $b4bb92ee48aa94b96ce93e9eb66358$var$_arrayLikeToArray(arr, len) {
    if (len == null || len > arr.length) len = arr.length;
    for (var i = 0, arr2 = new Array(len); i < len; i++) arr2[i] = arr[i];
    return arr2;
  }
  function $b4bb92ee48aa94b96ce93e9eb66358$var$_classCallCheck(instance, Constructor) {
    if (!(instance instanceof Constructor)) {
      throw new TypeError("Cannot call a class as a function");
    }
  }
  function $b4bb92ee48aa94b96ce93e9eb66358$var$_defineProperties(target, props) {
    for (var i = 0; i < props.length; i++) {
      var descriptor = props[i];
      descriptor.enumerable = descriptor.enumerable || false;
      descriptor.configurable = true;
      if ("value" in descriptor) descriptor.writable = true;
      Object.defineProperty(target, $b4bb92ee48aa94b96ce93e9eb66358$var$_toPropertyKey(descriptor.key), descriptor);
    }
  }
  function $b4bb92ee48aa94b96ce93e9eb66358$var$_createClass(Constructor, protoProps, staticProps) {
    if (protoProps) $b4bb92ee48aa94b96ce93e9eb66358$var$_defineProperties(Constructor.prototype, protoProps);
    if (staticProps) $b4bb92ee48aa94b96ce93e9eb66358$var$_defineProperties(Constructor, staticProps);
    Object.defineProperty(Constructor, "prototype", {
      writable: false
    });
    return Constructor;
  }
  function $b4bb92ee48aa94b96ce93e9eb66358$var$_inherits(subClass, superClass) {
    if (typeof superClass !== "function" && superClass !== null) {
      throw new TypeError("Super expression must either be null or a function");
    }
    subClass.prototype = Object.create(superClass && superClass.prototype, {
      constructor: {
        value: subClass,
        writable: true,
        configurable: true
      }
    });
    Object.defineProperty(subClass, "prototype", {
      writable: false
    });
    if (superClass) $b4bb92ee48aa94b96ce93e9eb66358$var$_setPrototypeOf(subClass, superClass);
  }
  function $b4bb92ee48aa94b96ce93e9eb66358$var$_setPrototypeOf(o, p) {
    $b4bb92ee48aa94b96ce93e9eb66358$var$_setPrototypeOf = Object.setPrototypeOf ? Object.setPrototypeOf.bind() : function _setPrototypeOf(o, p) {
      o.__proto__ = p;
      return o;
    };
    return $b4bb92ee48aa94b96ce93e9eb66358$var$_setPrototypeOf(o, p);
  }
  function $b4bb92ee48aa94b96ce93e9eb66358$var$_createSuper(Derived) {
    var hasNativeReflectConstruct = $b4bb92ee48aa94b96ce93e9eb66358$var$_isNativeReflectConstruct();
    return function _createSuperInternal() {
      var Super = $b4bb92ee48aa94b96ce93e9eb66358$var$_getPrototypeOf(Derived),
        result;
      if (hasNativeReflectConstruct) {
        var NewTarget = $b4bb92ee48aa94b96ce93e9eb66358$var$_getPrototypeOf(this).constructor;
        result = Reflect.construct(Super, arguments, NewTarget);
      } else {
        result = Super.apply(this, arguments);
      }
      return $b4bb92ee48aa94b96ce93e9eb66358$var$_possibleConstructorReturn(this, result);
    };
  }
  function $b4bb92ee48aa94b96ce93e9eb66358$var$_possibleConstructorReturn(self, call) {
    if (call && ($b4bb92ee48aa94b96ce93e9eb66358$var$_typeof(call) === "object" || typeof call === "function")) {
      return call;
    } else if (call !== void 0) {
      throw new TypeError("Derived constructors may only return object or undefined");
    }
    return $b4bb92ee48aa94b96ce93e9eb66358$var$_assertThisInitialized(self);
  }
  function $b4bb92ee48aa94b96ce93e9eb66358$var$_assertThisInitialized(self) {
    if (self === void 0) {
      throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
    }
    return self;
  }
  function $b4bb92ee48aa94b96ce93e9eb66358$var$_isNativeReflectConstruct() {
    if (typeof Reflect === "undefined" || !Reflect.construct) return false;
    if (Reflect.construct.sham) return false;
    if (typeof Proxy === "function") return true;
    try {
      Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {}));
      return true;
    } catch (e) {
      return false;
    }
  }
  function $b4bb92ee48aa94b96ce93e9eb66358$var$_getPrototypeOf(o) {
    $b4bb92ee48aa94b96ce93e9eb66358$var$_getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf.bind() : function _getPrototypeOf(o) {
      return o.__proto__ || Object.getPrototypeOf(o);
    };
    return $b4bb92ee48aa94b96ce93e9eb66358$var$_getPrototypeOf(o);
  }
  function $b4bb92ee48aa94b96ce93e9eb66358$var$_defineProperty(obj, key, value) {
    key = $b4bb92ee48aa94b96ce93e9eb66358$var$_toPropertyKey(key);
    if (key in obj) {
      Object.defineProperty(obj, key, {
        value: value,
        enumerable: true,
        configurable: true,
        writable: true
      });
    } else {
      obj[key] = value;
    }
    return obj;
  }
  function $b4bb92ee48aa94b96ce93e9eb66358$var$_toPropertyKey(t) {
    var i = $b4bb92ee48aa94b96ce93e9eb66358$var$_toPrimitive(t, "string");
    return "symbol" == $b4bb92ee48aa94b96ce93e9eb66358$var$_typeof(i) ? i : String(i);
  }
  function $b4bb92ee48aa94b96ce93e9eb66358$var$_toPrimitive(t, r) {
    if ("object" != $b4bb92ee48aa94b96ce93e9eb66358$var$_typeof(t) || !t) return t;
    var e = t[Symbol.toPrimitive];
    if (void 0 !== e) {
      var i = e.call(t, r || "default");
      if ("object" != $b4bb92ee48aa94b96ce93e9eb66358$var$_typeof(i)) return i;
      throw new TypeError("@@toPrimitive must return a primitive value.");
    }
    return ("string" === r ? String : Number)(t);
  }
  var $b4bb92ee48aa94b96ce93e9eb66358$var$AddRecipeView = /*#__PURE__*/function (_View) {
    $b4bb92ee48aa94b96ce93e9eb66358$var$_inherits(AddRecipeView, _View);
    var _super = $b4bb92ee48aa94b96ce93e9eb66358$var$_createSuper(AddRecipeView);
    function AddRecipeView() {
      var _this;
      $b4bb92ee48aa94b96ce93e9eb66358$var$_classCallCheck(this, AddRecipeView);
      _this = _super.call(this);
      $b4bb92ee48aa94b96ce93e9eb66358$var$_defineProperty($b4bb92ee48aa94b96ce93e9eb66358$var$_assertThisInitialized(_this), "_parentElement", document.querySelector('.upload'));
      $b4bb92ee48aa94b96ce93e9eb66358$var$_defineProperty($b4bb92ee48aa94b96ce93e9eb66358$var$_assertThisInitialized(_this), "_window", document.querySelector('.add-recipe-window'));
      $b4bb92ee48aa94b96ce93e9eb66358$var$_defineProperty($b4bb92ee48aa94b96ce93e9eb66358$var$_assertThisInitialized(_this), "_overlay", document.querySelector('.overlay'));
      $b4bb92ee48aa94b96ce93e9eb66358$var$_defineProperty($b4bb92ee48aa94b96ce93e9eb66358$var$_assertThisInitialized(_this), "_message", 'Recipe was sucessfully Uploaded');
      $b4bb92ee48aa94b96ce93e9eb66358$var$_defineProperty($b4bb92ee48aa94b96ce93e9eb66358$var$_assertThisInitialized(_this), "_btnOpen", document.querySelector('.nav__btn--add-recipe'));
      $b4bb92ee48aa94b96ce93e9eb66358$var$_defineProperty($b4bb92ee48aa94b96ce93e9eb66358$var$_assertThisInitialized(_this), "_btnClose", document.querySelector('.btn--close-modal'));
      _this._addHandlerShowWindow();
      _this._addHandlerHideWindow();
      return _this;
    }
    $b4bb92ee48aa94b96ce93e9eb66358$var$_createClass(AddRecipeView, [{
      key: "_addHandlerShowWindow",
      value: function _addHandlerShowWindow() {
        this._btnOpen.addEventListener('click', this.toggleWindow.bind(this));
      }

      // _addHandlerShowWindow(){
      //     this._btnOpen.addEventListener('click',function(e){
      //         this._overlay.classList.toggle('hidden');
      //         this._window.classList.toggle('hidden');
      //     })

      //     // if return like this then _overlay and _window will be search inside the btn_open
      //     // but we want to have in the class so use a bind by binding this class variables 
      //     // to the toggleMethod which will take this class overaly and window.
      // }
    }, {
      key: "toggleWindow",
      value: function toggleWindow() {
        this._overlay.classList.toggle('hidden');
        this._window.classList.toggle('hidden');
      }
    }, {
      key: "_addHandlerHideWindow",
      value: function _addHandlerHideWindow() {
        this._btnClose.addEventListener('click', this.toggleWindow.bind(this));
        this._overlay.addEventListener('click', this.toggleWindow.bind(this));
        // By binding the overlay and window of this class
      }
    }, {
      key: "addHandlerUpload",
      value: function addHandlerUpload(handler) {
        this._parentElement.addEventListener('submit', function (e) {
          e.preventDefault(); // default action will be cancelled.
          var dataArr = $b4bb92ee48aa94b96ce93e9eb66358$var$_toConsumableArray(new FormData(this));
          var data = Object.fromEntries(dataArr);
          handler(data);
        });
      }
    }, {
      key: "_generateMarkup",
      value: function _generateMarkup() {}
    }]);
    return AddRecipeView;
  }($b85584bbd00b4a0c9b1159737dbdca4b$export$default);
  var $b4bb92ee48aa94b96ce93e9eb66358$export$default = new $b4bb92ee48aa94b96ce93e9eb66358$var$AddRecipeView();
  function $dcda24f48c942085b4e752ea505f4e5$var$_typeof(o) {
    "@babel/helpers - typeof";

    return $dcda24f48c942085b4e752ea505f4e5$var$_typeof = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (o) {
      return typeof o;
    } : function (o) {
      return o && "function" == typeof Symbol && o.constructor === Symbol && o !== Symbol.prototype ? "symbol" : typeof o;
    }, $dcda24f48c942085b4e752ea505f4e5$var$_typeof(o);
  }
  function $dcda24f48c942085b4e752ea505f4e5$var$_regeneratorRuntime() {
    /*! regenerator-runtime -- Copyright (c) 2014-present, Facebook, Inc. -- license (MIT): https://github.com/facebook/regenerator/blob/main/LICENSE */
    $dcda24f48c942085b4e752ea505f4e5$var$_regeneratorRuntime = function _regeneratorRuntime() {
      return e;
    };
    var t,
      e = {},
      r = Object.prototype,
      n = r.hasOwnProperty,
      o = Object.defineProperty || function (t, e, r) {
        t[e] = r.value;
      },
      i = "function" == typeof Symbol ? Symbol : {},
      a = i.iterator || "@@iterator",
      c = i.asyncIterator || "@@asyncIterator",
      u = i.toStringTag || "@@toStringTag";
    function define(t, e, r) {
      return Object.defineProperty(t, e, {
        value: r,
        enumerable: !0,
        configurable: !0,
        writable: !0
      }), t[e];
    }
    try {
      define({}, "");
    } catch (t) {
      define = function define(t, e, r) {
        return t[e] = r;
      };
    }
    function wrap(t, e, r, n) {
      var i = e && e.prototype instanceof Generator ? e : Generator,
        a = Object.create(i.prototype),
        c = new Context(n || []);
      return o(a, "_invoke", {
        value: makeInvokeMethod(t, r, c)
      }), a;
    }
    function tryCatch(t, e, r) {
      try {
        return {
          type: "normal",
          arg: t.call(e, r)
        };
      } catch (t) {
        return {
          type: "throw",
          arg: t
        };
      }
    }
    e.wrap = wrap;
    var h = "suspendedStart",
      l = "suspendedYield",
      f = "executing",
      s = "completed",
      y = {};
    function Generator() {}
    function GeneratorFunction() {}
    function GeneratorFunctionPrototype() {}
    var p = {};
    define(p, a, function () {
      return this;
    });
    var d = Object.getPrototypeOf,
      v = d && d(d(values([])));
    v && v !== r && n.call(v, a) && (p = v);
    var g = GeneratorFunctionPrototype.prototype = Generator.prototype = Object.create(p);
    function defineIteratorMethods(t) {
      ["next", "throw", "return"].forEach(function (e) {
        define(t, e, function (t) {
          return this._invoke(e, t);
        });
      });
    }
    function AsyncIterator(t, e) {
      function invoke(r, o, i, a) {
        var c = tryCatch(t[r], t, o);
        if ("throw" !== c.type) {
          var u = c.arg,
            h = u.value;
          return h && "object" == $dcda24f48c942085b4e752ea505f4e5$var$_typeof(h) && n.call(h, "__await") ? e.resolve(h.__await).then(function (t) {
            invoke("next", t, i, a);
          }, function (t) {
            invoke("throw", t, i, a);
          }) : e.resolve(h).then(function (t) {
            u.value = t, i(u);
          }, function (t) {
            return invoke("throw", t, i, a);
          });
        }
        a(c.arg);
      }
      var r;
      o(this, "_invoke", {
        value: function value(t, n) {
          function callInvokeWithMethodAndArg() {
            return new e(function (e, r) {
              invoke(t, n, e, r);
            });
          }
          return r = r ? r.then(callInvokeWithMethodAndArg, callInvokeWithMethodAndArg) : callInvokeWithMethodAndArg();
        }
      });
    }
    function makeInvokeMethod(e, r, n) {
      var o = h;
      return function (i, a) {
        if (o === f) throw new Error("Generator is already running");
        if (o === s) {
          if ("throw" === i) throw a;
          return {
            value: t,
            done: !0
          };
        }
        for (n.method = i, n.arg = a;;) {
          var c = n.delegate;
          if (c) {
            var u = maybeInvokeDelegate(c, n);
            if (u) {
              if (u === y) continue;
              return u;
            }
          }
          if ("next" === n.method) n.sent = n._sent = n.arg;else if ("throw" === n.method) {
            if (o === h) throw o = s, n.arg;
            n.dispatchException(n.arg);
          } else "return" === n.method && n.abrupt("return", n.arg);
          o = f;
          var p = tryCatch(e, r, n);
          if ("normal" === p.type) {
            if (o = n.done ? s : l, p.arg === y) continue;
            return {
              value: p.arg,
              done: n.done
            };
          }
          "throw" === p.type && (o = s, n.method = "throw", n.arg = p.arg);
        }
      };
    }
    function maybeInvokeDelegate(e, r) {
      var n = r.method,
        o = e.iterator[n];
      if (o === t) return r.delegate = null, "throw" === n && e.iterator.return && (r.method = "return", r.arg = t, maybeInvokeDelegate(e, r), "throw" === r.method) || "return" !== n && (r.method = "throw", r.arg = new TypeError("The iterator does not provide a '" + n + "' method")), y;
      var i = tryCatch(o, e.iterator, r.arg);
      if ("throw" === i.type) return r.method = "throw", r.arg = i.arg, r.delegate = null, y;
      var a = i.arg;
      return a ? a.done ? (r[e.resultName] = a.value, r.next = e.nextLoc, "return" !== r.method && (r.method = "next", r.arg = t), r.delegate = null, y) : a : (r.method = "throw", r.arg = new TypeError("iterator result is not an object"), r.delegate = null, y);
    }
    function pushTryEntry(t) {
      var e = {
        tryLoc: t[0]
      };
      1 in t && (e.catchLoc = t[1]), 2 in t && (e.finallyLoc = t[2], e.afterLoc = t[3]), this.tryEntries.push(e);
    }
    function resetTryEntry(t) {
      var e = t.completion || {};
      e.type = "normal", delete e.arg, t.completion = e;
    }
    function Context(t) {
      this.tryEntries = [{
        tryLoc: "root"
      }], t.forEach(pushTryEntry, this), this.reset(!0);
    }
    function values(e) {
      if (e || "" === e) {
        var r = e[a];
        if (r) return r.call(e);
        if ("function" == typeof e.next) return e;
        if (!isNaN(e.length)) {
          var o = -1,
            i = function next() {
              for (; ++o < e.length;) if (n.call(e, o)) return next.value = e[o], next.done = !1, next;
              return next.value = t, next.done = !0, next;
            };
          return i.next = i;
        }
      }
      throw new TypeError($dcda24f48c942085b4e752ea505f4e5$var$_typeof(e) + " is not iterable");
    }
    return GeneratorFunction.prototype = GeneratorFunctionPrototype, o(g, "constructor", {
      value: GeneratorFunctionPrototype,
      configurable: !0
    }), o(GeneratorFunctionPrototype, "constructor", {
      value: GeneratorFunction,
      configurable: !0
    }), GeneratorFunction.displayName = define(GeneratorFunctionPrototype, u, "GeneratorFunction"), e.isGeneratorFunction = function (t) {
      var e = "function" == typeof t && t.constructor;
      return !!e && (e === GeneratorFunction || "GeneratorFunction" === (e.displayName || e.name));
    }, e.mark = function (t) {
      return Object.setPrototypeOf ? Object.setPrototypeOf(t, GeneratorFunctionPrototype) : (t.__proto__ = GeneratorFunctionPrototype, define(t, u, "GeneratorFunction")), t.prototype = Object.create(g), t;
    }, e.awrap = function (t) {
      return {
        __await: t
      };
    }, defineIteratorMethods(AsyncIterator.prototype), define(AsyncIterator.prototype, c, function () {
      return this;
    }), e.AsyncIterator = AsyncIterator, e.async = function (t, r, n, o, i) {
      void 0 === i && (i = Promise);
      var a = new AsyncIterator(wrap(t, r, n, o), i);
      return e.isGeneratorFunction(r) ? a : a.next().then(function (t) {
        return t.done ? t.value : a.next();
      });
    }, defineIteratorMethods(g), define(g, u, "Generator"), define(g, a, function () {
      return this;
    }), define(g, "toString", function () {
      return "[object Generator]";
    }), e.keys = function (t) {
      var e = Object(t),
        r = [];
      for (var n in e) r.push(n);
      return r.reverse(), function next() {
        for (; r.length;) {
          var t = r.pop();
          if (t in e) return next.value = t, next.done = !1, next;
        }
        return next.done = !0, next;
      };
    }, e.values = values, Context.prototype = {
      constructor: Context,
      reset: function reset(e) {
        if (this.prev = 0, this.next = 0, this.sent = this._sent = t, this.done = !1, this.delegate = null, this.method = "next", this.arg = t, this.tryEntries.forEach(resetTryEntry), !e) for (var r in this) "t" === r.charAt(0) && n.call(this, r) && !isNaN(+r.slice(1)) && (this[r] = t);
      },
      stop: function stop() {
        this.done = !0;
        var t = this.tryEntries[0].completion;
        if ("throw" === t.type) throw t.arg;
        return this.rval;
      },
      dispatchException: function dispatchException(e) {
        if (this.done) throw e;
        var r = this;
        function handle(n, o) {
          return a.type = "throw", a.arg = e, r.next = n, o && (r.method = "next", r.arg = t), !!o;
        }
        for (var o = this.tryEntries.length - 1; o >= 0; --o) {
          var i = this.tryEntries[o],
            a = i.completion;
          if ("root" === i.tryLoc) return handle("end");
          if (i.tryLoc <= this.prev) {
            var c = n.call(i, "catchLoc"),
              u = n.call(i, "finallyLoc");
            if (c && u) {
              if (this.prev < i.catchLoc) return handle(i.catchLoc, !0);
              if (this.prev < i.finallyLoc) return handle(i.finallyLoc);
            } else if (c) {
              if (this.prev < i.catchLoc) return handle(i.catchLoc, !0);
            } else {
              if (!u) throw new Error("try statement without catch or finally");
              if (this.prev < i.finallyLoc) return handle(i.finallyLoc);
            }
          }
        }
      },
      abrupt: function abrupt(t, e) {
        for (var r = this.tryEntries.length - 1; r >= 0; --r) {
          var o = this.tryEntries[r];
          if (o.tryLoc <= this.prev && n.call(o, "finallyLoc") && this.prev < o.finallyLoc) {
            var i = o;
            break;
          }
        }
        i && ("break" === t || "continue" === t) && i.tryLoc <= e && e <= i.finallyLoc && (i = null);
        var a = i ? i.completion : {};
        return a.type = t, a.arg = e, i ? (this.method = "next", this.next = i.finallyLoc, y) : this.complete(a);
      },
      complete: function complete(t, e) {
        if ("throw" === t.type) throw t.arg;
        return "break" === t.type || "continue" === t.type ? this.next = t.arg : "return" === t.type ? (this.rval = this.arg = t.arg, this.method = "return", this.next = "end") : "normal" === t.type && e && (this.next = e), y;
      },
      finish: function finish(t) {
        for (var e = this.tryEntries.length - 1; e >= 0; --e) {
          var r = this.tryEntries[e];
          if (r.finallyLoc === t) return this.complete(r.completion, r.afterLoc), resetTryEntry(r), y;
        }
      },
      catch: function _catch(t) {
        for (var e = this.tryEntries.length - 1; e >= 0; --e) {
          var r = this.tryEntries[e];
          if (r.tryLoc === t) {
            var n = r.completion;
            if ("throw" === n.type) {
              var o = n.arg;
              resetTryEntry(r);
            }
            return o;
          }
        }
        throw new Error("illegal catch attempt");
      },
      delegateYield: function delegateYield(e, r, n) {
        return this.delegate = {
          iterator: values(e),
          resultName: r,
          nextLoc: n
        }, "next" === this.method && (this.arg = t), y;
      }
    }, e;
  }
  function $dcda24f48c942085b4e752ea505f4e5$var$asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) {
    try {
      var info = gen[key](arg);
      var value = info.value;
    } catch (error) {
      reject(error);
      return;
    }
    if (info.done) {
      resolve(value);
    } else {
      Promise.resolve(value).then(_next, _throw);
    }
  }
  function $dcda24f48c942085b4e752ea505f4e5$var$_asyncToGenerator(fn) {
    return function () {
      var self = this,
        args = arguments;
      return new Promise(function (resolve, reject) {
        var gen = fn.apply(self, args);
        function _next(value) {
          $dcda24f48c942085b4e752ea505f4e5$var$asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value);
        }
        function _throw(err) {
          $dcda24f48c942085b4e752ea505f4e5$var$asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err);
        }
        _next(undefined);
      });
    };
  }
  var $dcda24f48c942085b4e752ea505f4e5$var$controlledRecipe = /*#__PURE__*/function () {
    var _ref = $dcda24f48c942085b4e752ea505f4e5$var$_asyncToGenerator( /*#__PURE__*/$dcda24f48c942085b4e752ea505f4e5$var$_regeneratorRuntime().mark(function _callee() {
      var _window, id;
      return $dcda24f48c942085b4e752ea505f4e5$var$_regeneratorRuntime().wrap(function _callee$(_context) {
        while (1) switch (_context.prev = _context.next) {
          case 0:
            _context.prev = 0;
            id = (_window = window) === null || _window === void 0 || (_window = _window.location) === null || _window === void 0 ? void 0 : _window.hash.slice(1);
            if (id) {
              _context.next = 4;
              break;
            }
            return _context.abrupt("return");
          case 4:
            $f620f1e41831269f29485288a0a683$export$default.renderSpinner();
            $fc10709d24e4a04b1b69f091ac5fc55$export$default.update($c3b5db471f8fb616d9495023ad42cee1$export$getSearchResultsPage());
            $f29b5555c6852e3bbcd9784393e0c58a$export$default.update($c3b5db471f8fb616d9495023ad42cee1$export$state.bookmarks);
            _context.next = 9;
            return $c3b5db471f8fb616d9495023ad42cee1$export$loadRecipe(id);
          case 9:
            // Rendering receipe
            $f620f1e41831269f29485288a0a683$export$default.render($c3b5db471f8fb616d9495023ad42cee1$export$state.recipe);
            _context.next = 16;
            break;
          case 12:
            _context.prev = 12;
            _context.t0 = _context["catch"](0);
            $f620f1e41831269f29485288a0a683$export$default.renderError();
            console.error(_context.t0);
          case 16:
          case "end":
            return _context.stop();
        }
      }, _callee, null, [[0, 12]]);
    }));
    return function controlledRecipe() {
      return _ref.apply(this, arguments);
    };
  }();
  var $dcda24f48c942085b4e752ea505f4e5$var$controlSearchResults = /*#__PURE__*/function () {
    var _ref2 = $dcda24f48c942085b4e752ea505f4e5$var$_asyncToGenerator( /*#__PURE__*/$dcda24f48c942085b4e752ea505f4e5$var$_regeneratorRuntime().mark(function _callee2() {
      var query;
      return $dcda24f48c942085b4e752ea505f4e5$var$_regeneratorRuntime().wrap(function _callee2$(_context2) {
        while (1) switch (_context2.prev = _context2.next) {
          case 0:
            _context2.prev = 0;
            $fc10709d24e4a04b1b69f091ac5fc55$export$default.renderSpinner();
            // take a query from the field.
            query = $f91c83782128716ccea27a6a$export$default.getQuery();
            if (query) {
              _context2.next = 5;
              break;
            }
            return _context2.abrupt("return");
          case 5:
            _context2.next = 7;
            return $c3b5db471f8fb616d9495023ad42cee1$export$loadSearchResults(query);
          case 7:
            // Render the result in the left pannel.
            console.log($c3b5db471f8fb616d9495023ad42cee1$export$state.search.result);
            // resultView.render(state.search.result);
            $fc10709d24e4a04b1b69f091ac5fc55$export$default.render($c3b5db471f8fb616d9495023ad42cee1$export$getSearchResultsPage());

            // Render intitial Pagination buttons
            $b363fc27ed313e62ddd6f8021836b71$export$default.render($c3b5db471f8fb616d9495023ad42cee1$export$state.search);
            _context2.next = 15;
            break;
          case 12:
            _context2.prev = 12;
            _context2.t0 = _context2["catch"](0);
            throw _context2.t0;
          case 15:
          case "end":
            return _context2.stop();
        }
      }, _callee2, null, [[0, 12]]);
    }));
    return function controlSearchResults() {
      return _ref2.apply(this, arguments);
    };
  }();
  var $dcda24f48c942085b4e752ea505f4e5$var$controlPagination = function controlPagination(btn) {
    var nextPage = +btn.dataset.goto; // adding + will make it integer otherwise converting to binary.
    $fc10709d24e4a04b1b69f091ac5fc55$export$default.render($c3b5db471f8fb616d9495023ad42cee1$export$getSearchResultsPage(nextPage));
    $b363fc27ed313e62ddd6f8021836b71$export$default.render($c3b5db471f8fb616d9495023ad42cee1$export$state.search);
  };
  var $dcda24f48c942085b4e752ea505f4e5$var$controlServings = function controlServings(btn) {
    var newServings = +btn.dataset.updateTo;
    //update the recipe servings in state.

    if (newServings > 0) {
      $c3b5db471f8fb616d9495023ad42cee1$export$updateServings(newServings);

      // update the recipeview
      $f620f1e41831269f29485288a0a683$export$default.update($c3b5db471f8fb616d9495023ad42cee1$export$state.recipe);
    }
  };
  var $dcda24f48c942085b4e752ea505f4e5$var$controlAddBookmark = function controlAddBookmark() {
    // Add/ remove  a bookmarked
    if (!$c3b5db471f8fb616d9495023ad42cee1$export$state.recipe.bookmarked) $c3b5db471f8fb616d9495023ad42cee1$export$addBookmark($c3b5db471f8fb616d9495023ad42cee1$export$state.recipe);else $c3b5db471f8fb616d9495023ad42cee1$export$deleteBookmark($c3b5db471f8fb616d9495023ad42cee1$export$state.recipe.id);

    // update the  recipe view
    $f620f1e41831269f29485288a0a683$export$default.update($c3b5db471f8fb616d9495023ad42cee1$export$state.recipe);

    // render bookmarked.
    $f29b5555c6852e3bbcd9784393e0c58a$export$default.render($c3b5db471f8fb616d9495023ad42cee1$export$state.bookmarks);
  };
  var $dcda24f48c942085b4e752ea505f4e5$var$controlBookmarks = function controlBookmarks() {
    // THis is done because when page will be trying to update the bookmarks, bookmarks need to present
    // otherwise it will throw error.
    $f29b5555c6852e3bbcd9784393e0c58a$export$default.render($c3b5db471f8fb616d9495023ad42cee1$export$state.bookmarks);
  };
  var $dcda24f48c942085b4e752ea505f4e5$var$controlAddRecipe = /*#__PURE__*/function () {
    var _ref3 = $dcda24f48c942085b4e752ea505f4e5$var$_asyncToGenerator( /*#__PURE__*/$dcda24f48c942085b4e752ea505f4e5$var$_regeneratorRuntime().mark(function _callee3(newRecipe) {
      return $dcda24f48c942085b4e752ea505f4e5$var$_regeneratorRuntime().wrap(function _callee3$(_context3) {
        while (1) switch (_context3.prev = _context3.next) {
          case 0:
            _context3.prev = 0;
            $b4bb92ee48aa94b96ce93e9eb66358$export$default.renderSpinner();
            // store in the forkify app
            _context3.next = 4;
            return $c3b5db471f8fb616d9495023ad42cee1$export$uploadRecipe(newRecipe);
          case 4:
            console.log($c3b5db471f8fb616d9495023ad42cee1$export$state.recipe);
            $b4bb92ee48aa94b96ce93e9eb66358$export$default.renderMessage();

            // render the recipe 
            $f620f1e41831269f29485288a0a683$export$default.render($c3b5db471f8fb616d9495023ad42cee1$export$state.recipe);
            $f29b5555c6852e3bbcd9784393e0c58a$export$default.render($c3b5db471f8fb616d9495023ad42cee1$export$state.bookmarks);
            window.history.pushState(null, '', "#".concat($c3b5db471f8fb616d9495023ad42cee1$export$state.recipe.id));

            // close the form.
            //  setTimeout(function(){
            //   // addRecipeView.toggleWindow();
            //  }, MODEL_CLOSE_SEC*1000);
            _context3.next = 15;
            break;
          case 11:
            _context3.prev = 11;
            _context3.t0 = _context3["catch"](0);
            console.error(_context3.t0);
            $b4bb92ee48aa94b96ce93e9eb66358$export$default.renderError(_context3.t0.message);
          case 15:
          case "end":
            return _context3.stop();
        }
      }, _callee3, null, [[0, 11]]);
    }));
    return function controlAddRecipe(_x) {
      return _ref3.apply(this, arguments);
    };
  }();
  var $dcda24f48c942085b4e752ea505f4e5$var$init = function init() {
    $f620f1e41831269f29485288a0a683$export$default.addHandlerRender($dcda24f48c942085b4e752ea505f4e5$var$controlledRecipe);
    $f91c83782128716ccea27a6a$export$default.addHandlerSearch($dcda24f48c942085b4e752ea505f4e5$var$controlSearchResults);
    $b363fc27ed313e62ddd6f8021836b71$export$default.addHandlerClick($dcda24f48c942085b4e752ea505f4e5$var$controlPagination);
    $f620f1e41831269f29485288a0a683$export$default.addHandlerUpdateServings($dcda24f48c942085b4e752ea505f4e5$var$controlServings);
    $f620f1e41831269f29485288a0a683$export$default.addHandlerAddBookmark($dcda24f48c942085b4e752ea505f4e5$var$controlAddBookmark);
    $f29b5555c6852e3bbcd9784393e0c58a$export$default.addHandlerRender($dcda24f48c942085b4e752ea505f4e5$var$controlBookmarks);
    $b4bb92ee48aa94b96ce93e9eb66358$export$default.addHandlerUpload($dcda24f48c942085b4e752ea505f4e5$var$controlAddRecipe);
  };
  $dcda24f48c942085b4e752ea505f4e5$var$init();
})();
//# sourceMappingURL=controller.9c9e4af0.js.map
