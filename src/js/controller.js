import { loadRecipe, state, loadSearchResults, getSearchResultsPage,updateServings, addBookmark, deleteBookmark, uploadRecipe } from './model';
import recipeView from './views/recipeView';
import resultView from './views/resultView';
import searchView from './views/searchView';
import PaginationView from './views/PaginationView';
import bookMarkedView from './views/bookMarkedView';
import addRecipeView from './views/addRecipeView';
import { MODEL_CLOSE_SEC } from './config';
const controlledRecipe = async function(){
  try{
    const id = window?.location?.hash.slice(1);
    if(!id)
    return ;
    recipeView.renderSpinner();

    
    resultView.update(getSearchResultsPage());

    bookMarkedView.update(state.bookmarks)
    
    await loadRecipe(id);

    // Rendering receipe
    recipeView.render(state.recipe);
  }
  catch(err){
    recipeView.renderError();
    console.error(err);
  }
}

const controlSearchResults = async function(){
  try{

    resultView.renderSpinner();
    // take a query from the field.
    const query= searchView.getQuery();
    if(!query)
    return;
    // if query exist then fetch it from the URL.
    await loadSearchResults(query);

    // Render the result in the left pannel.
    console.log(state.search.result);
    // resultView.render(state.search.result);
    resultView.render(getSearchResultsPage());

    // Render intitial Pagination buttons
    PaginationView.render(state.search);
  }
  catch(err){
    throw err;
  }
}

const controlPagination = function(btn){
  const nextPage = +btn.dataset.goto; // adding + will make it integer otherwise converting to binary.
  resultView.render(getSearchResultsPage(nextPage));
  PaginationView.render(state.search)
  
}


const controlServings = function(btn){

  const newServings =+btn.dataset.updateTo;
  //update the recipe servings in state.

  if(newServings >0){
  updateServings(newServings);

  // update the recipeview
  recipeView.update(state.recipe);
  }
}


const controlAddBookmark  = function(){

  // Add/ remove  a bookmarked
  if(!state.recipe.bookmarked)
  addBookmark(state.recipe);
  else 
  deleteBookmark(state.recipe.id);

  // update the  recipe view
  recipeView.update(state.recipe);

  // render bookmarked.
  bookMarkedView.render(state.bookmarks);
}

const controlBookmarks =  function(){

  // THis is done because when page will be trying to update the bookmarks, bookmarks need to present
  // otherwise it will throw error.
  bookMarkedView.render(state.bookmarks);
}

const controlAddRecipe = async function(newRecipe){
  try{

    addRecipeView.renderSpinner();
      // store in the forkify app
     await  uploadRecipe(newRecipe);

     console.log(state.recipe);

     addRecipeView.renderMessage();

     // render the recipe 
     recipeView.render(state.recipe);


     bookMarkedView.render(state.bookmarks);

     window.history.pushState(null,'',`#${state.recipe.id}`);

     // close the form.
    //  setTimeout(function(){
    //   // addRecipeView.toggleWindow();
    //  }, MODEL_CLOSE_SEC*1000);
  }
  catch(err){
    console.error(err);
    addRecipeView.renderError(err.message);
  }

}
const init = function(){
  recipeView.addHandlerRender(controlledRecipe);
  searchView.addHandlerSearch(controlSearchResults);
  PaginationView.addHandlerClick(controlPagination)
  recipeView.addHandlerUpdateServings(controlServings);
  recipeView.addHandlerAddBookmark(controlAddBookmark);
  bookMarkedView.addHandlerRender(controlBookmarks);
  addRecipeView.addHandlerUpload(controlAddRecipe);
}

init();