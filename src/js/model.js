import { API_URL, RESULT_PER_PAGE } from "./config";
import { AJAX } from "./helpers";
import { KEY } from "./config";
export const state= {
    recipe:{},
    search:{
        query:'',
        result:[], 
        page: 1,
        resultPerPage: RESULT_PER_PAGE,
    },
    bookmarks:[],
    
};

const createRecipeObject = function(data){
    let recipeData = data.data.recipe;
    return {  
      id: recipeData.id,
      title: recipeData.title,
      publisher: recipeData.publisher,
      sourceUrl: recipeData.source_url,
      image: recipeData.image_url,
      servings: recipeData.servings,
      cookingTime: recipeData.cooking_time,
      ingredients: recipeData.ingredients,
      ...(recipeData.key && {key: recipeData.key}),
    }
}
 
export const loadRecipe = async function(id){
try{
    const data = await AJAX(`${API_URL}${id}?key=${KEY}`); 

    state.recipe= createRecipeObject(data);
    if(state.bookmarks.some(bookmark => bookmark.id ===id))
        state.recipe.bookmarked= true;
    else 
        state.recipe.bookmarked= false;
}
catch(err){
    throw err;
}

}


export const loadSearchResults = async function(query){
    try{
        const data = await AJAX(`${API_URL}?search=${query}&key=${KEY}`);
        state.search.query= query;

        state.search.result= data?.data?.recipes.map((recipeData)=>{
            return  {
                id: recipeData.id,
                title: recipeData.title,
                publisher: recipeData.publisher,
                image: recipeData.image_url,
                ...(recipeData.key && {key: recipeData.key}),
            }
        })
        state.search.page=1;
    }
    catch(err){
        throw err;
    }
}

export const getSearchResultsPage = function(page = state.search.page){

    state.search.page = page;
    const start= (page-1)*state.search.resultPerPage;
    const end= page *state.search.resultPerPage ;

    return state.search.result.slice(start,end);

}


export const updateServings = function(newServings){

      state.recipe.ingredients.forEach((ing)=>{
        ing.quantity= (newServings*ing.quantity/state.recipe.servings);
      })
      state.recipe.servings= newServings;
}

const presistBookmarks = function(){
    localStorage.setItem('bookmarks', JSON.stringify(state.bookmarks));
}

export const addBookmark = function(recipe){
    // Add boomark

    state.bookmarks.push(recipe);

    //bookmarked current recipe as true
    // if(recipe.id === state.recipe.id)
    state.recipe.bookmarked =true;

    presistBookmarks();
}

export const deleteBookmark = function (id){
    const index = state.bookmarks.findIndex(ele => ele.id===id)
    state.bookmarks.splice(index,1);


    // if(id === state.recipe.id)
    state.recipe.bookmarked=false;
    console.log(state.bookmarks);

    presistBookmarks();
}

const clearBookmark = function(){
    localStorage.clear('bookmark');
}

const init = function(){
    const storage = localStorage.getItem('bookmarks');
    if(storage)
    state.bookmarks = JSON.parse(storage);
};


init();
//clearBookmark();
// console.log(state.bookmarks);

export const uploadRecipe = async function( newRecipe){
 
    try{
        let ingredients = Object.entries(newRecipe).filter((entry)=>{
            return entry[0].startsWith("ingredient") && entry[1] !== ''
        })

        ingredients=ingredients.map((ing)=>{

            const ingArr= ing[1].replaceAll(' ', '').split(',')
            if(ingArr.length!==3 )
            throw new Error('Wrong ingredient format! Please use the correct format');

            const [quantity,unit,description]= ingArr;
            return {quantity: quantity? +quantity : null, unit, description}
        })

        const recipe={  
            title: newRecipe.title,
            publisher: newRecipe.publisher,
            source_url: newRecipe.sourceUrl,
            image_url: newRecipe.image,
            servings: +newRecipe.servings,
            cooking_time: +newRecipe.cookingTime,
            ingredients: ingredients
          }
          console.log(recipe);
        const data = await AJAX(`${API_URL}?key=${KEY}`, recipe);
        state.recipe= createRecipeObject(data);
        addBookmark(state.recipe);
    }
    catch(err){
        throw err;
    }
}