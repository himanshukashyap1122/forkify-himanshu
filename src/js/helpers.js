import { TIMEOUT_TIME } from "./config";
const timeout = function (s) {
    return new Promise(function (_, reject) {
      setTimeout(function () {
        reject(new Error(`Request took too long! Timeout after ${s} second`));
      }, s * 1000);
    });
  };

// export const getJSON = async function(url){
//     try{
//         const fetchPromise =   await fetch(url);
//         const res = await Promise.race([fetchPromise,timeout(TIMEOUT_TIME)]);
//         const data= await res.json();
//         if(!res.ok)
//         throw new Error(`${data.error} ${res.status}` +' status code');
//         return data;
//     }
//     catch(err){
//         throw err;
//     }
// }

// export const sendJSON = async function(url, uploadRecipe){
//   try{
//       const fetchPromise = fetch(url,{
//         method: 'POST',
//         headers: {
//           'Content-Type' : 'application/json',
//         },
//         body: JSON.stringify(uploadRecipe),
//       });

//       const res = await Promise.race([fetchPromise,timeout(TIMEOUT_TIME)]);
//       const data= await res.json();
//       if(!res.ok)
//       throw new Error(`${data.error} ${res.status}` +' status code');
//       return data;
//   }
//   catch(err){
//       throw err;
//   }
// }


export const AJAX= async function(url, uploadRecipe=undefined){

  try{
    const fetchPromise = uploadRecipe ? fetch(url,{
      method: 'POST',
      headers: {
        'Content-Type' : 'application/json',
      },
      body: JSON.stringify(uploadRecipe),
    }) : fetch(url);
    const res = await Promise.race([fetchPromise,timeout(TIMEOUT_TIME)]);
    const data= await res.json();
    if(!res.ok)
    throw new Error(`${data.error} ${res.status}` +' status code');
    return data;
  }
  catch(err){
    throw err;
  }

}

