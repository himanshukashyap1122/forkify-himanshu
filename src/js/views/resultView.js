import View from "./View";
import icons from 'url:../../img/icons.svg'
import previewView from "./previewView";
class ResultView extends View{
    _parentElement= document.querySelector('.results');
    _errorMessage= 'No recipes found for the given query!'

    _generateMarkup() {
      return this._data.map(result => previewView.render(result, false)).join('');
    }
}

export default new ResultView();