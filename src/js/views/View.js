
import icons from 'url:../../img/icons.svg'
class View {
  /**
   * render the received object to the dom
   * @params{Object} data
   * {*} render
   */
_data;
    render(data, render = true){

        if(!data || (Array.isArray(data) && data.length ===0))
        return this.renderError();
        this._data= data;


        const markup = this._generateMarkup();

        if(!render)
        return markup;
      
        this._clear();
        this._parentElement.insertAdjacentHTML('afterbegin',markup);
    }

    _clear(){
        this._parentElement.innerHTML= '';
    }

    update (data){
      
        this._data= data;
        const newMarkup = this._generateMarkup();

        // https://medium.com/tech-learnings/web-apis-document-createdocumentfragment-range-createcontextualfragment-d6d7254690e9#id_token=eyJhbGciOiJSUzI1NiIsImtpZCI6IjFmNDBmMGE4ZWYzZDg4MDk3OGRjODJmMjVjM2VjMzE3YzZhNWI3ODEiLCJ0eXAiOiJKV1QifQ.eyJpc3MiOiJodHRwczovL2FjY291bnRzLmdvb2dsZS5jb20iLCJhenAiOiIyMTYyOTYwMzU4MzQtazFrNnFlMDYwczJ0cDJhMmphbTRsamRjbXMwMHN0dGcuYXBwcy5nb29nbGV1c2VyY29udGVudC5jb20iLCJhdWQiOiIyMTYyOTYwMzU4MzQtazFrNnFlMDYwczJ0cDJhMmphbTRsamRjbXMwMHN0dGcuYXBwcy5nb29nbGV1c2VyY29udGVudC5jb20iLCJzdWIiOiIxMDkwODk3NzE3NTE5MzEyMDk4MjAiLCJlbWFpbCI6ImhpbWFuc2h1a2FzaHlhcDExMjJAZ21haWwuY29tIiwiZW1haWxfdmVyaWZpZWQiOnRydWUsIm5iZiI6MTcwNTE1ODIzNiwibmFtZSI6IkhpbWFuc2h1IEthc2h5YXAiLCJwaWN0dXJlIjoiaHR0cHM6Ly9saDMuZ29vZ2xldXNlcmNvbnRlbnQuY29tL2EvQUNnOG9jSm9aa0E5V1JGV2w4N2REbk1SS0MydHdIMEJQMy1scnROM1VFQnYtRFNPbVdJPXM5Ni1jIiwiZ2l2ZW5fbmFtZSI6IkhpbWFuc2h1IiwiZmFtaWx5X25hbWUiOiJLYXNoeWFwIiwibG9jYWxlIjoiZW4iLCJpYXQiOjE3MDUxNTg1MzYsImV4cCI6MTcwNTE2MjEzNiwianRpIjoiZDNjMzZjNmYyYWNhYTYxMWVkMGU4NmRkMGY4ODJiOWMxNzVjODViNSJ9.i2s_mQHoOqy9EtO6PoRIK-VvolWPcb_JVEw4nzJ75DWc8ETZlHTKCkT6q1EiU9KsTUPXCR7SmiL3S5lfHJ1f5_G0SkmFKl7ckCIAmrVx5zIgeVLhvKK_0rtCyomrZ4f2ZtEyBLcAgHBUmezPsVXVhXxvB1H5FFMhon7tntbJbLNkwg7_1tqctPsmDW4U8ZOde-pZCR1XL3jIyDGfQ4FD9qJKt4GdrSYl3UBkJosJfKw6fwVYgEWNeFtRI2Lm3MKBPnee7p2ppbT3A36fZWWNRwug6ic3XfMa-Fb-k2qCpKD1fqudrnpZGdlEINqxZIe-pfxgZ6LaNAc0i7Py7lX_oQ
        // Instead of adding the conents to the dom again, we can actually filter out what was change
        // and only change those in the dom which will make the page reload smooth.
        const newDom = document.createRange().createContextualFragment(newMarkup)

        const newElements = Array.from(newDom.querySelectorAll('*'));
        const currentElement = Array.from(this._parentElement.querySelectorAll('*'));

        //This will only work when we have a text.
        newElements.forEach((newEle,index)=>{
          const curEle = currentElement[index];
          if(curEle != undefined &&!newEle.isEqualNode(curEle) &&newEle.firstChild?.nodeValue.trim()!== '') {
          curEle.textContent = newEle.textContent;
          }
          if(curEle != undefined && !newEle.isEqualNode(curEle))
          // console.log(Array.from(newEle.attributes));
          Array.from(newEle.attributes).forEach((attr)=>{
            curEle.setAttribute(attr.name, attr.value);
          })
        })

        //Updated changed Attributes.

    }

    renderSpinner(){
        const markup= `
          <div class="spinner">
          <svg>
            <use href="${icons}#icon-loader"></use>
          </svg>
          </div>
       `;
       this._clear();
       this._parentElement.insertAdjacentHTML('afterbegin', markup);
      }


    renderError(message=this._errorMessage){
        const markup = `<div class="error">
        <div>
          <svg>
            <use href="${icons}#icon-alert-triangle"></use>
          </svg>
        </div>
        <p>${message}</p>
      </div>`

      this._clear();
      this._parentElement.insertAdjacentHTML('afterbegin', markup);
    }

    renderMessage(message=this._message){
        const markup = `<div class="message">
        <div>
          <svg>
            <use href="src/img/${icons}#icon-smile"></use>
          </svg>
        </div>
        <p>${message}</p>
      </div>`

      this._clear();
      this._parentElement.insertAdjacentHTML('afterbegin', markup);
    }
}


export default  View; 