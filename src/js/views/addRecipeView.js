import View from "./View";
import icons from 'url:../../img/icons.svg'

class AddRecipeView extends View{

    _parentElement = document.querySelector('.upload');
    _window = document.querySelector('.add-recipe-window');
    _overlay = document.querySelector('.overlay');
    _message= 'Recipe was sucessfully Uploaded';
    
    _btnOpen = document.querySelector('.nav__btn--add-recipe');
    _btnClose = document.querySelector('.btn--close-modal');


    constructor(){
        super();
        this._addHandlerShowWindow();
        this._addHandlerHideWindow();
    }
    _addHandlerShowWindow(){
        this._btnOpen.addEventListener('click', this.toggleWindow.bind(this))
    }
    
    // _addHandlerShowWindow(){
    //     this._btnOpen.addEventListener('click',function(e){
    //         this._overlay.classList.toggle('hidden');
    //         this._window.classList.toggle('hidden');
    //     })

    //     // if return like this then _overlay and _window will be search inside the btn_open
    //     // but we want to have in the class so use a bind by binding this class variables 
    //     // to the toggleMethod which will take this class overaly and window.
    // }
    
    toggleWindow(){
        this._overlay.classList.toggle('hidden');
        this._window.classList.toggle('hidden');
    }
    _addHandlerHideWindow(){
        this._btnClose.addEventListener('click',this.toggleWindow.bind(this));
        this._overlay.addEventListener('click',this.toggleWindow.bind(this));
        // By binding the overlay and window of this class
    }

    addHandlerUpload(handler){
        this._parentElement.addEventListener('submit',function(e){
            e.preventDefault(); // default action will be cancelled.
            const dataArr = [...new FormData(this)];
            const data = Object.fromEntries(dataArr);
            handler(data);
        })
    }
    _generateMarkup(){
        
    }
}

export default new AddRecipeView();