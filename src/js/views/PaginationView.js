import { RESULT_PER_PAGE } from "../config";
import View from "./View";
import icons from 'url:../../img/icons.svg'
class PaginationView extends View{
    _parentElement= document.querySelector('.pagination');


    addHandlerClick(handler){
        this._parentElement.addEventListener('click', function(e){
            const btn= e.target.closest('.btn--inline ');
            if(!btn)
            return;
            handler(btn);
        })
    }
    _generateMarkup(){

        const numPages= Math.ceil(this._data.result.length / this._data.resultPerPage);
        console.log(numPages)
        let  currentPage =this._data.page;
         
        if(currentPage===1 && numPages>1){
        return this._generateMarkupForwordButton(currentPage);
        }

        else if(currentPage=== numPages && numPages >1){
            return this._generateMarkupPreviousButton(currentPage);
        }
        if(currentPage < numPages){
            return  (this._generateMarkupPreviousButton(currentPage).concat(
                    this._generateMarkupForwordButton(currentPage)));
        }

    }
    _generateMarkupPreviousButton(currentPage){
        return ` 
        <button data-goto= "${currentPage-1}"class="btn--inline pagination__btn--prev">
            <svg class="search__icon">
            <use href="${icons}#icon-arrow-left"></use>
            </svg>
            <span>Page ${currentPage-1}</span>
        </button>
            `
    }
    _generateMarkupForwordButton(currentPage){
        return ` 
        <button data-goto= "${currentPage+1}" class="btn--inline pagination__btn--next">
            <span>Page ${currentPage+1}</span>
            <svg class="search__icon">
            <use href="${icons}#icon-arrow-right"></use>
            </svg>
        </button>
            `
    }
}

export default new PaginationView();