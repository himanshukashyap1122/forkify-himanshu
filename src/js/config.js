export const API_URL = 'https://forkify-api.herokuapp.com/api/v2/recipes/';
export const TIMEOUT_TIME= 10;
export const RESULT_PER_PAGE=10;
export const KEY= '9125ab40-f78e-4ebe-8183-131d0731992d';
export const MODEL_CLOSE_SEC=2.5;